#include <fstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>

using namespace std;
int main()
{

	double e=0, FR=0;
    ifstream fin_e("EvtEff_SCATTER_INI_FIN_4.txt"); 
    ifstream fin_FR("FR1_SCATTER_INI_FIN_4.txt"); 
    //ofstream e1("e1_SCATTER_INI_FIN_4.txt");
    ofstream e2("e2_SCATTER_INI_FIN_4.txt");
    ofstream e1;
    e1.open("e1_SCATTER_INI_FIN_4.txt");
  
	for (int count =0; count<209; count++)
	{
		fin_e>>e;
		fin_FR>>FR;
		e1<<e/FR<<endl;
		e2<<e-FR<<endl;	
	}
	e1.close();
	e2.close();
	fin_e.close();
	fin_FR.close();

  return 0;
}
