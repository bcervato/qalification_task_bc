#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.

c=?



################################################ Cos VAR ############################ HIGHT STATA
mkdir Cos_var 
cd Cos_var/
j=0
#j=0 #=default
i=-7
#z=4=default


mkdir Cos_var_Fin_0_cos_4
cd Cos_var_Fin_0_cos_4
for z in -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10
do


	mkdir Cos_$z
	cd Cos_$z
	cp /afs/cern.ch/user/b/bcervato/parallel/ForBeatrice/run/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	#cp /backup_store/i-7_jvar_z4/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	#cd Cos_$z
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST_?/Cos_var/Plots_Con_var_"$z".root
	cp EvtEff.txt ../../../../elaboration_ST_?/Cos_var/EvtEff_Cos_var_"$z".txt
	cp FR1.txt ../../../../elaboration_ST_?/Cos_var/FR1_Cos_var_"$z".txt
	cp FR2.txt ../../../../elaboration_ST_?/Cos_var/FR2_Cos_var_"$z".txt
	cp EvtEff_N.txt ../../../../elaboration_ST_?/Cos_var/EvtEff_Cos_var_N_"$z".txt
	cp FR1_N.txt ../../../../elaboration_ST_?/Cos_var/FR1_Cos_var_N_"$z".txt
	cp FR2_N.txt ../../../../elaboration_ST_?/Cos_var/FR2_Cos_var_N_"$z".txt
	cp EvtEff_D.txt ../../../../elaboration_ST_?/Cos_var/EvtEff_Cos_var_D_"$z".txt
	cp FR1_D.txt ../../../../elaboration_ST_?/Cos_var/FR1_Cos_var_D_"$z".txt
	cp FR2_D.txt ../../../../elaboration_ST_?/Cos_var/FR2_Cos_var_D_"$z".txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 

cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_?/Cos_var

#######################################           Cos VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	for c in "" "_N" "_D"
	do
		mv "$b"_Cos_var"$c"_-9.txt temp.txt 
		for z in -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 
		do
			cat temp.txt "$b"_Cos_var"$c"_"$z".txt > file3.txt
			mv file3.txt temp.txt
		done
		mv temp.txt "$b"_COS_VAR"$c"_"$i"_"$j".txt 
	done 
done


echo "The end..."

bash script_elaboration.sh
