#!/bin/sh
#Autor: Beatrice Cervato
#Changing some var. names to obtain the desidered analysis

#Please, copy the analysis that you whant to perform and paste it in the uncommented part of the script.
#REMEMBER to modify the value of j, i or z.....

echo "What analysis do you want to perform? Please, enter:"
echo "1 for cos variable, IniBDT = parameter"
echo "2 for cos variable, FinBDT = parameter"
echo "3 for FinBDT variable, cos = parameter"
echo "4 for FinBDT variable, IniBDT = parameter"
echo "5 for IniBDT variable, cos = parameter"
echo "6 for IniBDT variable, FinBDT = parameter"

#index = $(cat)
#read index






#if ($index="4") #VAR = Cos ##########################new######################
cp Backup_elaboration.C elaboration.C
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    i=-7 ######################################## MODIFY #####################################
    temp=1
    t=12
    j=0
    sed -i 's/float index=0/float index=4/g' elaboration.C
    sed -i 's/INPUT_FILE_1/'$b'_COS_VAR_'$i'_'$j'.txt/g' elaboration.C
    sed -i 's/INPUT_FILE_2/'$b'_COS_VAR_N_'$i'_'$j'.txt/g' elaboration.C
    sed -i 's/INPUT_FILE_3/'$b'_COS_VAR_D_'$i'_'$j'.txt/g' elaboration.C
    sed -i 's/MESSAGE_LEG_1/IniBDTCut = '$i'e-1 FinBDTCut = '$j'e-1/g' elaboration.C
    for f in 1 2 3 4 5
    do
        c=`expr $f - $temp`
        z=`expr $f + $f + $f + $f - $t`
        sed -i 's/Plots_'$f'.root/Plots_Con_var_'$z'.root/g' elaboration.C #Plots_Fin_var_'$i'_'$j'_'$z'.root/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/CosSVPV = '$z'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/CosSVPV/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Cos/g' elaboration.C
    sed -i 's/SAVE_NAME_EFF/'$b'_Var-Cos_EFF/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_COSVAR/g' elaboration.C
    sed -i 's/myfile.root/EC_'$b'.root/g' elaboration.C

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=4/float index=0/g' elaboration.C





echo "Fine prima parte"
cp Backup_correlation.C correlation.C
for g in "FR1" "FR2"
do
	echo "Ciao"
    ##########################    FRs VS Eff    ####################
	z=4 ######################################## MODIFY #####################################
    t=3
    j=0
    sed -i 's/INPUT_FILE_Y/'$g'_COS_VAR_'$i'_'$j'.txt/g' correlation.C
    sed -i 's/INPUT_FILE_X/EvtEff_COS_VAR_'$i'_'$j'.txt/g' correlation.C
    sed -i 's/MESSAGE_LEG_1/COSBDTCut = var IniBDT = '$i'e-1 FinBDT = '$j'e-1 /g' correlation.C
    sed -i 's/X_AXIS_LABEL/EvtEff/g' correlation.C
    sed -i 's/Y_AXIS_LABEL/1 - '$g'/g' correlation.C
    sed -i 's/SAVE_NAME/'$g'_Var-Cos_Correlation/g' correlation.C
    sed -i 's/ROOT_FILE_NAME/'$g'/g' correlation.C

    root -q .x correlation.C
    cp Backup_correlation.C correlation.C
done



echo "The end"
