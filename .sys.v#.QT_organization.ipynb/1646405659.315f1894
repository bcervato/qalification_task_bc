{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "766634bf",
   "metadata": {},
   "source": [
    "# Welcome in the QT working directory of Beatrice Cervato\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90992215",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\"> In this Notebook, you can find the description of my Qualification Task (QT) and a step-by-step guide concerning how to do the analysis that I performed. \n",
    "    In the following paragraphs, I describe how I usually run the code, producing (for the moment) some preliminary results concerning the efficiency estimation and the optimization of the JobOption file settings. Moreover, I am keeping track of the working flow, thus this notebook is continuously updated. Indeed, the basic idea is to have a specific script for each analysis topic that one could run to obtain a specific plot or set of plots. \n",
    "The scope of this notebook is also to give an example of the analysis implemented for the efficiency estimation. All the other analysis will be done with the same aim, thus it will be intuitive to run a code, developed for a specific pourpose, obtaining the relative results.  \n",
    "Summing up, in order to perform a specific analysis, I usually run a specific code (or better a set of scripts) storing the results in CernBox.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33d8a211",
   "metadata": {},
   "source": [
    "## QT description"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2a480fd",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\"> Several interesting physical processes lead to the production of low-energy (soft) b-quarks in the final state. The produced b-hadrons are also low energy and not bound to reconstructed jets. To select such processes, low-energy b-hadrons must be identified independently of the reconstructed jets. Standard FTAG algorithms are limited to the phase space inside reconstructed jets (typical cone size ~0.4 and less) and therefore are not able to detect soft B-hadron decays with a much wider decay product distribution. A tool for the inclusive reconstruction of all secondary vertices in an event has already been developed. The reconstructed B-hadron decay secondary vertices can be either associated with jets to improve the jet b-tagging efficiency itself (a functionality already exploited by CMS) or used independently of jets for an event with B-hadrons tagging/rejection.\n",
    "\n",
    "<div class=\"text-justify\"> The principle QT deliverables should be the optimisation of the selection efficiency and background suppression of the soft B-hadron vertex reconstruction tool and validation of its performance on data. The fake-vertex rate of the inclusive tool will be compared to the fake rate of the standard b-tagging vertex reconstruction inside jets. This should improve the understanding of the b-tagging fake rate and facilitate the light-jet rejection calibration.\n",
    "\n",
    "<div class=\"text-justify\"> You can find all the updates and further information concerning my QT at this [link](https://its.cern.ch/jira/browse/AFT-577). </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6a3610d",
   "metadata": {},
   "source": [
    "## Efficiency estimation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1fb7984",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\">In order to optimize the B-hadron tagging, it is crucial to have a code (*\"Eff.C\"*) that can calculate the efficiency of the code that reconstruct secondary verticies. The first analysis consists to find out the best value for the BH-pt cut. At the Athena level, I modify the value of the truth BH-pt cut and, then, I analyze the output files *trackExampleSV.hist.root*. Just running the script *script_efficiency_PtCut.sh*, it is possible to get the efficiency for three BH-pt cuts (1, 2 and 4 GeV). Moreover, this script produces also some summary-plots (stored in the *elaboration* folder) that are very usefull if one wants to perform a comparison between the trends of the distribution obtained with different cuts.  <div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ad878f4f",
   "metadata": {},
   "source": [
    "Please, use the following script in order to produce the BH-pt cut analysis:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "8e7bc99c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "BB_direct sample: Pt Cut = 1_GeV\n",
      "   ------------------------------------------------------------------\n",
      "  | Welcome to ROOT 6.24/06                        https://root.cern |\n",
      "  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |\n",
      "  | Built for linuxx8664gcc on Sep 02 2021, 14:20:23                 |\n",
      "  | From tags/v6-24-06@v6-24-06                                      |\n",
      "  | With g++ (GCC) 8.3.0                                             |\n",
      "  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |\n",
      "   ------------------------------------------------------------------\n",
      "\n",
      "Warning in <TFile::Init>: file trackExampleSV.hist.root probably not closed, trying to recover\n",
      "Warning in <TFile::Init>: no keys recovered, file has been made a Zombie\n",
      "(Eff &) @0x7f598f50a018\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "BB_direct sample: Pt Cut = 2_GeV\n",
      "   ------------------------------------------------------------------\n",
      "  | Welcome to ROOT 6.24/06                        https://root.cern |\n",
      "  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |\n",
      "  | Built for linuxx8664gcc on Sep 02 2021, 14:20:23                 |\n",
      "  | From tags/v6-24-06@v6-24-06                                      |\n",
      "  | With g++ (GCC) 8.3.0                                             |\n",
      "  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |\n",
      "   ------------------------------------------------------------------\n",
      "\n",
      "(Eff &) @0x7fb22713e018\n",
      " Overall B-hadron detection efficiency per event =0.505061\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "BB_direct sample: Pt Cut = 4_GeV\n",
      "   ------------------------------------------------------------------\n",
      "  | Welcome to ROOT 6.24/06                        https://root.cern |\n",
      "  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |\n",
      "  | Built for linuxx8664gcc on Sep 02 2021, 14:20:23                 |\n",
      "  | From tags/v6-24-06@v6-24-06                                      |\n",
      "  | With g++ (GCC) 8.3.0                                             |\n",
      "  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |\n",
      "   ------------------------------------------------------------------\n",
      "\n",
      "(Eff &) @0x7f98d20c4018\n",
      " Overall B-hadron detection efficiency per event =0.922154\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "ttbar sample: Pt Cut = 1_GeV\n",
      "   ------------------------------------------------------------------\n",
      "  | Welcome to ROOT 6.24/06                        https://root.cern |\n",
      "  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |\n",
      "  | Built for linuxx8664gcc on Sep 02 2021, 14:20:23                 |\n",
      "  | From tags/v6-24-06@v6-24-06                                      |\n",
      "  | With g++ (GCC) 8.3.0                                             |\n",
      "  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |\n",
      "   ------------------------------------------------------------------\n",
      "\n",
      "(Eff &) @0x7ff65767f018\n",
      " Overall B-hadron detection efficiency per event =0.746177\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "ttbar sample: Pt Cut = 2_GeV\n",
      "   ------------------------------------------------------------------\n",
      "  | Welcome to ROOT 6.24/06                        https://root.cern |\n",
      "  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |\n",
      "  | Built for linuxx8664gcc on Sep 02 2021, 14:20:23                 |\n",
      "  | From tags/v6-24-06@v6-24-06                                      |\n",
      "  | With g++ (GCC) 8.3.0                                             |\n",
      "  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |\n",
      "   ------------------------------------------------------------------\n",
      "\n",
      "(Eff &) @0x7f40288ea018\n",
      " Overall B-hadron detection efficiency per event =0.768826\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "ttbar sample: Pt Cut = 4_GeV\n",
      "   ------------------------------------------------------------------\n",
      "  | Welcome to ROOT 6.24/06                        https://root.cern |\n",
      "  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |\n",
      "  | Built for linuxx8664gcc on Sep 02 2021, 14:20:23                 |\n",
      "  | From tags/v6-24-06@v6-24-06                                      |\n",
      "  | With g++ (GCC) 8.3.0                                             |\n",
      "  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |\n",
      "   ------------------------------------------------------------------\n",
      "\n",
      "(Eff &) @0x7febfbcb8018\n",
      " Overall B-hadron detection efficiency per event =0.817619\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "Warning in <TApplication::GetOptions>: macro .x not found\n",
      "   ------------------------------------------------------------------\n",
      "  | Welcome to ROOT 6.24/06                        https://root.cern |\n",
      "  | (c) 1995-2021, The ROOT Team; conception: R. Brun, F. Rademakers |\n",
      "  | Built for linuxx8664gcc on Sep 02 2021, 14:20:23                 |\n",
      "  | From tags/v6-24-06@v6-24-06                                      |\n",
      "  | With g++ (GCC) 8.3.0                                             |\n",
      "  | Try '.help', '.demo', '.license', '.credits', '.quit'/'.q'       |\n",
      "   ------------------------------------------------------------------\n",
      "\n",
      "\n",
      "Applying ATLAS style settings...\n",
      "\n",
      "\n",
      "Processing elaboration.C...\n",
      "\n",
      "Applying ATLAS style settings...\n",
      "\n",
      "Info in <TCanvas::Print>: png file Different_pt_cut_eta_comparison.png has been created\n",
      "Info in <TCanvas::Print>: png file Different_pt_cut_pt_comparison.png has been created\n",
      "Info in <TCanvas::Print>: png file Eta_samples_comparison_1GeV.png has been created\n",
      "Info in <TCanvas::Print>: png file Pt_samples_comparison_1GeV.png has been created\n"
     ]
    }
   ],
   "source": [
    "!bash script_efficiency_PtCut.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d64d5f3a",
   "metadata": {},
   "source": [
    "## JobOption file optimization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56cb34f1",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "eea1cfbe",
   "metadata": {},
   "source": [
    "## Folder organization\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94bf3bc7",
   "metadata": {},
   "source": [
    " In this folder, called QT, you can find all the results of my QT. In different folder are made different analysis or calculation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ee6639a",
   "metadata": {},
   "source": [
    "### ForBeatrice"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac6eb153",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\">This is the main folder for the QT analysis. Indeed, it contains the code that, using the SVTool and the BJetSVFinder, produce two output files called *trackExampleSV.hist.root* and *Results.root* that are used for further analysis.</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc17b2b4",
   "metadata": {},
   "source": [
    "### EfficiencyPtCut"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fc5d3e5",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\"> This folder contains six *trackExampleSV.hist.root* files subdivided in to 2 subfolders, called **BB_direct** and **ttbar**, which in turn contain 3 folders, one for each cut over the transverse momentum of the B-hadron. \n",
    "    For each Pt cut and sample, running the bash script *script.sh*, I can calculate the efficiency and store all the most important plots in the file called *Plots.root*. For a more accurate definition of the efficiency, please move to the dedicated section. </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd921dbe",
   "metadata": {},
   "source": [
    "### elaboration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6436fd65",
   "metadata": {},
   "source": [
    "Here you can find the scripts that allows to make some comparison beetween different sample or B-Hadron-Pt cut, producing the final results. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
