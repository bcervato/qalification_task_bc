{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "766634bf",
   "metadata": {},
   "source": [
    "# Welcome in the QT repository of Beatrice Cervato\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5847af8",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\"> Hi, here you can find an hight level description of my Qualification Task. In the following paragraph, you can find how I usually run the code, producing (for the moment) some preliminary results concerning the efficiency estimation. </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33d8a211",
   "metadata": {},
   "source": [
    "## QT description"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2a480fd",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\"> Several interesting physical processes lead to the production of low-energy (soft) b-quarks in the final state. The produced b-hadrons are also low energy and not bound to reconstructed jets. To select such processes, low-energy b-hadrons must be identified independently of the reconstructed jets. Standard FTAG algorithms are limited to the phase space inside reconstructed jets (typical cone size ~0.4 and less) and therefore are not able to detect soft B-hadron decays with a much wider decay product distribution. A tool for the inclusive reconstruction of all secondary vertices in an event has already been developed. The reconstructed B-hadron decay secondary vertices can be either associated with jets to improve the jet b-tagging efficiency itself (a functionality already exploited by CMS) or used independently of jets for an event with B-hadrons tagging/rejection.\n",
    "\n",
    "<div class=\"text-justify\"> The principle QT deliverables should be the optimisation of the selection efficiency and background suppression of the soft B-hadron vertex reconstruction tool and validation of its performance on data. The fake-vertex rate of the inclusive tool will be compared to the fake rate of the standard b-tagging vertex reconstruction inside jets. This should improve the understanding of the b-tagging fake rate and facilitate the light-jet rejection calibration.\n",
    "\n",
    "<div class=\"text-justify\"> You can find all the updates and further information concerning my QT at this [link](https://its.cern.ch/jira/browse/AFT-577). </div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6a3610d",
   "metadata": {},
   "source": [
    "## Folder organization"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1fb7984",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\">In this repository, called \"QT\", you can find all the results of my QT. In different folder are made different analysis or calculation. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ee6639a",
   "metadata": {},
   "source": [
    "### ForBeatrice"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac6eb153",
   "metadata": {},
   "source": [
    "<div class=\"text-justify\">This is the main folder for the QT analysis. Indeed, it contains the job that, using the SVTool and the BJetSVFinder, produce an output file called trackExampleSV.hist.root that is the one used for further analysis.</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dc17b2b4",
   "metadata": {},
   "source": [
    "### EfficiencyPtCut"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fc5d3e5",
   "metadata": {},
   "source": [
    "In this folder you can find the root output file coming from the *TrackExampleSV* algorithm. The results are subdivided in to 2 subfolders called **BB_direct** and **ttbar** which contain 3 folders, one for each cut over the transverse momentum of the B-hadron. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "07539f04",
   "metadata": {},
   "source": [
    "### elaboration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ca4d027",
   "metadata": {},
   "source": [
    "Here you can find the scripts that allows to make some comparison beetween different sample or B-Hadron-Pt cut"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72c74b7f",
   "metadata": {},
   "source": [
    "## Run the job"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0839a40c",
   "metadata": {},
   "source": [
    "Please, use the following scripts in order to produce the results exactly as I did."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 122,
   "id": "905ed865",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "BB_direct sample: Pt Cut = 1_GeV\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "BB_direct sample: Pt Cut = 2_GeV\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "BB_direct sample: Pt Cut = 4_GeV\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "ttbar sample: Pt Cut = 1_GeV\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "ttbar sample: Pt Cut = 2_GeV\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n",
      "ttbar sample: Pt Cut = 4_GeV\n",
      "========================================================================================\n",
      "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"
     ]
    }
   ],
   "source": [
    "#!cd ForBeatrice/run/;cp trackExampleSV.hist.root ../../Efficiency_PtCut/ttbar/2_GeV/;cd ../../Efficiency_PtCut/ttbar/2_GeV;bash script.sh\n",
    "!bash script.sh"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 101,
   "id": "3a7f2e25",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/eos/home-b/bcervato/SWAN_projects/QT\r\n"
     ]
    }
   ],
   "source": [
    "!pwd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2a239b1",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
