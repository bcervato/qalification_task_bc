//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jul  6 15:53:13 2022 by ROOT version 6.24/02
// from TTree Event/Event
// found on file: trackExampleSV.hist.root
//////////////////////////////////////////////////////////

#ifndef Eff_tuning_h
#define Eff_tuning_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class Eff_tuning {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           ntTrk;
   Int_t           ntHFtrue;
   Int_t           ntHFvrt;
   Int_t           ntLFvrt;
   Int_t           ntHFvrt1;
   Int_t           ntLFvrt1;
   Int_t           nVrt;
   Float_t         dRVrtBH[6];   //[nVrt]
   Float_t         dRMomBH[6];   //[nVrt]
   Int_t           matchVrtBH[6];   //[nVrt]
   Float_t         VrtPt[6];   //[nVrt]
   Float_t         VrtMinPtT[6];   //[nVrt]
   Float_t         VrtM[6];   //[nVrt]
   Float_t         VrtR[6];   //[nVrt]
   Float_t         VrtDR2[6];   //[nVrt]
   Int_t           VrtTrk[6];   //[nVrt]
   Int_t           VrtTrkHF[6];   //[nVrt]
   Int_t           nBHad;
   Float_t         BHadPt[8];   //[nBHad]
   Float_t         BHadEta[8];   //[nBHad]
   Int_t           nLLP;
   Float_t         llpR[1];   //[nLLP]
   Float_t         llpPhi[1];   //[nLLP]
   Int_t           llpIndV[1];   //[nLLP]
   Float_t         llpDR2[1];   //[nLLP]
   Int_t           njets;
   Float_t         jetPt[6];   //[njets]
   Float_t         jetmv2c10[6];   //[njets]
   Int_t           OutPtCut;

   // List of branches
   TBranch        *b_ntTrk;   //!
   TBranch        *b_ntHFtrue;   //!
   TBranch        *b_ntHFvrt;   //!
   TBranch        *b_ntLFvrt;   //!
   TBranch        *b_ntHFvrt1;   //!
   TBranch        *b_ntLFvrt1;   //!
   TBranch        *b_nVrt;   //!
   TBranch        *b_dRVrtBH;   //!
   TBranch        *b_dRMomBH;   //!
   TBranch        *b_matchVrtBH;   //!
   TBranch        *b_VrtPt;   //!
   TBranch        *b_VrtMinPtT;   //!
   TBranch        *b_VrtM;   //!
   TBranch        *b_VrtR;   //!
   TBranch        *b_VrtDR2;   //!
   TBranch        *b_VrtTrk;   //!
   TBranch        *b_VrtTrkHF;   //!
   TBranch        *b_nBHad;   //!
   TBranch        *b_BHadPt;   //!
   TBranch        *b_BHadEta;   //!
   TBranch        *b_nLLP;   //!
   TBranch        *b_llpR;   //!
   TBranch        *b_llpPhi;   //!
   TBranch        *b_llpIndV;   //!
   TBranch        *b_llpDR2;   //!
   TBranch        *b_njets;   //!
   TBranch        *b_jetPt;   //!
   TBranch        *b_jetmv2c10;   //!
   TBranch        *b_OutPtCut;   //!

   Eff_tuning(TTree *tree=0);
   virtual ~Eff_tuning();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Eff_tuning_cxx
Eff_tuning::Eff_tuning(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("trackExampleSV.hist.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("trackExampleSV.hist.root");
      }
      f->GetObject("Event",tree);

   }
   Init(tree);
}

Eff_tuning::~Eff_tuning()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Eff_tuning::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Eff_tuning::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Eff_tuning::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("ntTrk", &ntTrk, &b_ntTrk);
   fChain->SetBranchAddress("ntHFtrue", &ntHFtrue, &b_ntHFtrue);
   fChain->SetBranchAddress("ntHFvrt", &ntHFvrt, &b_ntHFvrt);
   fChain->SetBranchAddress("ntLFvrt", &ntLFvrt, &b_ntLFvrt);
   fChain->SetBranchAddress("ntHFvrt1", &ntHFvrt1, &b_ntHFvrt1);
   fChain->SetBranchAddress("ntLFvrt1", &ntLFvrt1, &b_ntLFvrt1);
   fChain->SetBranchAddress("nVrt", &nVrt, &b_nVrt);
   fChain->SetBranchAddress("dRVrtBH", dRVrtBH, &b_dRVrtBH);
   fChain->SetBranchAddress("dRMomBH", dRMomBH, &b_dRMomBH);
   fChain->SetBranchAddress("matchVrtBH", matchVrtBH, &b_matchVrtBH);
   fChain->SetBranchAddress("VrtPt", VrtPt, &b_VrtPt);
   fChain->SetBranchAddress("VrtMinPtT", VrtMinPtT, &b_VrtMinPtT);
   fChain->SetBranchAddress("VrtM", VrtM, &b_VrtM);
   fChain->SetBranchAddress("VrtR", VrtR, &b_VrtR);
   fChain->SetBranchAddress("VrtDR2", VrtDR2, &b_VrtDR2);
   fChain->SetBranchAddress("VrtTrk", VrtTrk, &b_VrtTrk);
   fChain->SetBranchAddress("VrtTrkHF", VrtTrkHF, &b_VrtTrkHF);
   fChain->SetBranchAddress("nBHad", &nBHad, &b_nBHad);
   fChain->SetBranchAddress("BHadPt", BHadPt, &b_BHadPt);
   fChain->SetBranchAddress("BHadEta", BHadEta, &b_BHadEta);
   fChain->SetBranchAddress("nLLP", &nLLP, &b_nLLP);
   fChain->SetBranchAddress("llpR", &llpR, &b_llpR);
   fChain->SetBranchAddress("llpPhi", &llpPhi, &b_llpPhi);
   fChain->SetBranchAddress("llpIndV", &llpIndV, &b_llpIndV);
   fChain->SetBranchAddress("llpDR2", &llpDR2, &b_llpDR2);
   fChain->SetBranchAddress("njets", &njets, &b_njets);
   fChain->SetBranchAddress("jetPt", jetPt, &b_jetPt);
   fChain->SetBranchAddress("jetmv2c10", jetmv2c10, &b_jetmv2c10);
   fChain->SetBranchAddress("OutPtCut", &OutPtCut, &b_OutPtCut);
   Notify();
}

Bool_t Eff_tuning::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Eff_tuning::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Eff_tuning::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Eff_tuning_cxx
