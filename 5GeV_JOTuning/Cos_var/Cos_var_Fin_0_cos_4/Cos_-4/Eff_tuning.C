#define Eff_tuning_cxx
#include "Eff_tuning.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <fstream>
#include <TFile.h>
#include <TColor.h>
#include <iostream>
#include <TImage.h>
#include <TEfficiency.h>

void Eff_tuning::Loop()
{
if (fChain == 0) return;
	Long64_t nentries = fChain->GetEntriesFast();
	
   TH1F * h_bPtAll =new TH1F("bPtAll","All Bhadron Pt",50,0.,2.e4);
   TH1F * h_bPtMat =new TH1F("bPtMat","Matched Bhadron Pt",50,0.,2.e4);
   TH1F * h_bPtEff =new TH1F("bPtEff","Bhadron Efficiency vs Pt",50,0.,2.e4);
    
   h_bPtEff->GetXaxis()->SetTitle("BH Pt [MeV]");
   h_bPtEff->GetYaxis()->SetTitle("Efficiency");
   
   Long64_t nbytes = 0, nb = 0;   

   float SV_bad_2=0, nVrt_2=0, Sum_VrtTrkHF=0, SV_bad=0, SV_bad_tot=0, SV_good_tot=0, SV_good=0, Evt_all_fake=0, BHtot=0, NOVrt=0, out_eta=0, OutPtCut_counter=0, nVrtTOT=0, VrtTrk0=0, VrtTrk0TOT=0, nonMatchedSVTOT=0, Evnt_BHless=0, Evnt_BHless_temp=0, VrtTrk_non_0=0;
   int nEventsWithMatchedSV=0; //Number of events with at least one SV matched to b-hadron
   int NonAcc=0, NonAccTot=0, count_non_reco=0, event_non_reco=0;
   ofstream fout("EvtEff.txt");
   ofstream fout1("FR1.txt");
   ofstream fout2("FR2.txt");  
   ofstream foutBH("BHEff.txt");
   
   ofstream fout1N("FR1_N.txt");
   ofstream fout1D("FR1_D.txt");
   ofstream fout2N("FR2_N.txt");
   ofstream fout2D("FR2_D.txt");
   ofstream foutN("EvtEff_N.txt");
   ofstream foutD("EvtEff_D.txt");
    
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      
      VrtTrk0=0;
      SV_good=0;
      SV_bad_2=0;
      nVrt_2=0;
      Sum_VrtTrkHF=0;
      for(int iv=0; iv<nVrt; iv++){
		  Sum_VrtTrkHF+=VrtTrkHF[iv];
		  
		if (dRVrtBH[iv]<0.6 && VrtTrkHF[iv]>0 && fabs(BHadEta[matchVrtBH[iv]])<2.5)
		{
			SV_good++;			        			    // count number of SVs matched to a b-hadron
			h_bPtMat->Fill(BHadPt[matchVrtBH[iv]],1.);  // Pt of the matched b-hadron for the given SV
		}	
		else
			SV_bad_2++;
		if (VrtTrkHF[iv]>0)	
			nVrt_2++;

      }
      SV_good_tot+=SV_good;     //Numerator of BH-eff
      if(nVrt>0 && Sum_VrtTrkHF==0)
		Evt_all_fake++; //Numerator of FR1
      if(nVrt>0 && Sum_VrtTrkHF>0)
		SV_bad_tot+=SV_bad_2; //Numerator of FR2
	  if (nVrt>0 && Sum_VrtTrkHF>0)
		nVrtTOT+=nVrt_2; //Denominator of FR2
      if(SV_good>0)
          nEventsWithMatchedSV++;   //Number of events with at least one SV matched to b-hadron = Numerator of Evt_eff

      if(nBHad==0) 
		Evnt_BHless++;
      else
      {
		  Evnt_BHless_temp=0;
		  for(int ib=0; ib<nBHad; ib++){
			  if (fabs(BHadEta[ib])<2.5 && BHadPt[ib]>2000){
				  h_bPtAll->Fill(BHadPt[ib],1.);  // Fill pt distribution of ALL b-hadrons
				  BHtot++; //Denominator of BH-eff
			  }
			  else
				  Evnt_BHless_temp++; 
			}
			if (Evnt_BHless_temp==nBHad)
				Evnt_BHless++;
      }
    }  //--End of event loop
    
    
	h_bPtEff->Divide(h_bPtMat, h_bPtAll, 1.,1.,"O"); // Calculate ratio of histograms - pt-dependent B-hadron efficiency
    std::cout<<" Eff_BH = (number of good SV) / (number of BH with pt and eta constraint) = "<<(float)(SV_good_tot)/(float)(BHtot)<<'\n';
    std::cout<<" Overall B-hadron detection efficiency per event = Eff_evt = (number of events in which at least one SV is good) / (total number of events with BH) = "<<(float)nEventsWithMatchedSV/((float)(nentries)-Evnt_BHless)<<'\n';
    std::cout<<" Number of event without BH or in wich |BH_eta| > 2.5 = "<<Evnt_BHless<<" over a total amount of events = "<<nentries<<'\n';
    std::cout<<" Fake Rate (1) = (# Events in which all SV are fakes)/(# Events with BH) = "<<(float)Evt_all_fake/((float)(nentries)-Evnt_BHless)<<'\n';
    std::cout<<" Fake Rate (2) = (Total number of bad SV in events with at least 1 good SV)/(total number of SV in events with at least 1 good SV) = "<<(float)SV_bad_tot/((float)(nVrtTOT))<<'\n';
    fout<<(float)nEventsWithMatchedSV/((float)(nentries)-Evnt_BHless)<<'\n';
    fout1<<(float)Evt_all_fake/((float)(nentries)-Evnt_BHless)<<'\n';
    fout2<<(float)SV_bad_tot/((float)(nVrtTOT))<<'\n';
	foutBH<<(float)(SV_good_tot)/BHtot<<'\n';
		
	foutN<<	nEventsWithMatchedSV<<'\n';
	foutD<<nentries-Evnt_BHless<<'\n';
	fout1N<<Evt_all_fake<<'\n';
	fout1D<<nentries-Evnt_BHless<<'\n';
	fout2N<<SV_bad_tot<<'\n';
	fout2D<<nVrtTOT<<'\n';
	
	TEfficiency* pEff =0;
	//TEfficiency* pEff = new TEfficiency("bPtAll_clone","Reco-efficiency;BH Pt [MeV];#epsilon",50,0.,2.e4,100,0,0.5);
	TFile saving ("Plots.root","RECREATE");
	if(TEfficiency::CheckConsistency(*h_bPtMat,*h_bPtAll))
	{ 
	  //pEff->SetTotalHistogram(*h_bPtAll, "f");
	  //pEff->SetPassedHistogram(*h_bPtMat, "f");
	  pEff = new TEfficiency(*h_bPtMat,*h_bPtAll);
	  //pEff->Draw("");
	  pEff->Write();
	}  
   //h_bPtEff->Draw();
   h_bPtEff->SetOption();
   h_bPtEff->Write(); 
   h_bPtMat->Write();
   h_bPtAll->Write();

 
   saving.Close();
}
