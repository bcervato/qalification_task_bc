#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.

c=0

cd /data/atlastop3/cervato/qt/elaboration_ST_5GeV/
mkdir Scatter_Ini_Fin
cd /data/atlastop3/cervato/qt/5GeV_JOTuning/



################################################ Fin and Ini VAR ############################ 
mkdir Scatter_Ini_Fin 
cd Scatter_Ini_Fin/
#i=-8
#j=0 #=default
z=4 
#z=4=default

mkdir Scatter_Ini_Fin_cos_4
cd Scatter_Ini_Fin_cos_4/
for j in -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10
do
	mkdir Fin_$j
	cd Fin_$j
	for i in $(seq -9 $j);
	do	
		mkdir Ini_$i
		cd Ini_$i
		cp /nfs/morris_4/data/atlastop3/cervato/ForBeatrice/run/Scatter_Ini_Fin_analysis/5GeV_NEW_CALIBRATION/$c/trackExampleSV_$c.hist.root trackExampleSV.hist.root
		cp ../../../../*.sh .
		cp ../../../../Eff_tuning.C ./Eff_tuning1.C
		echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
		bash Constructor_MakeClass.sh 
		mv Eff_tuning1.C Eff_tuning.C
		bash script_MakeClass.sh
		echo "========================================================================================"
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		cp Plots.root ../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/Plots_Scatter_Ini_Fin_"$j"_"$i".root
		cp EvtEff.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/EvtEff_Scatter_Ini_Fin_"$j"_"$i".txt
		cp FR1.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/FR1_Scatter_Ini_Fin_"$j"_"$i".txt
		cp FR2.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/FR2_Scatter_Ini_Fin_"$j"_"$i".txt
		cp EvtEff_N.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/EvtEff_Scatter_Ini_Fin_N_"$j"_"$i".txt
		cp FR1_N.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/FR1_Scatter_Ini_Fin_N_"$j"_"$i".txt
		cp FR2_N.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/FR2_Scatter_Ini_Fin_N_"$j"_"$i".txt
		cp EvtEff_D.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/EvtEff_Scatter_Ini_Fin_D_"$j"_"$i".txt
		cp FR1_D.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/FR1_Scatter_Ini_Fin_D_"$j"_"$i".txt
		cp FR2_D.txt ../../../../../elaboration_ST_5GeV/Scatter_Ini_Fin/FR2_Scatter_Ini_Fin_D_"$j"_"$i".txt
		cd ../
		d=$c
		c=`expr $d + 1`
	done
	cd ../
done 
cd /data/atlastop3/cervato/qt/elaboration_ST_5GeV/Scatter_Ini_Fin/



########################################           Fin and Ini VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	for c in "" "_N" "_D"
	do
		mv "$b"_Scatter_Ini_Fin"$c"_-9_-9.txt temp.txt 
		for j in -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 
		do
			for i in $(seq -9 $j);
			do
				cat temp.txt "$b"_Scatter_Ini_Fin"$c"_"$j"_"$i".txt > file3.txt
				mv file3.txt temp.txt
			done	
		done
		mv temp.txt "$b"_SCATTER_INI_FIN"$c"_"$z".txt 
	done 
done

echo "The end..."
