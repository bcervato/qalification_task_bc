#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 6 parameters in the JobOption file.

c=0


cd /data/atlastop3/cervato/qt/elaboration_ST_5/
mkdir Rad_var
cd /data/atlastop3/cervato/qt/5_JOTuning/


################################################ Rad VAR ############################ HIGHT STATA
mkdir Rad_var 
cd Rad_var/
j=0
#j=0 #=default
z=4
i=-8 
#z=4=default


mkdir Rad_var_Ini_-8_Fin_0_cos_4
cd Rad_var_Ini_-8_Fin_0_cos_4
for r in 0 1 2 3 4 5 6 7 8 9
do


	mkdir Rad_$r
	cd Rad_$r
	cp /nfs/morris_4/data/atlastop3/cervato/ForBeatrice/run/OUTPUT/Rad_var/5GeV/trackExampleSV_"$c".hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z, radius = $r"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	#++++++++++++++++++++++++++
<<COMMENT
	cd Rad_$r
	cp ../../../Eff_tuning.C .
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash script_MakeClass.sh
COMMENT
	#++++++++++++++++++++++

	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST_5/Rad_var/Plots_Rad_var_"$r".root
	cp EvtEff.txt ../../../../elaboration_ST_5/Rad_var/EvtEff_Rad_var_"$r".txt
	cp FR1.txt ../../../../elaboration_ST_5/Rad_var/FR1_Rad_var_"$r".txt
	cp FR2.txt ../../../../elaboration_ST_5/Rad_var/FR2_Rad_var_"$r".txt
	cp EvtEff_N.txt ../../../../elaboration_ST_5/Rad_var/EvtEff_Rad_var_N_"$r".txt
	cp FR1_N.txt ../../../../elaboration_ST_5/Rad_var/FR1_Rad_var_N_"$r".txt
	cp FR2_N.txt ../../../../elaboration_ST_5/Rad_var/FR2_Rad_var_N_"$r".txt
	cp EvtEff_D.txt ../../../../elaboration_ST_5/Rad_var/EvtEff_Rad_var_D_"$r".txt
	cp FR1_D.txt ../../../../elaboration_ST_5/Rad_var/FR1_Rad_var_D_"$r".txt
	cp FR2_D.txt ../../../../elaboration_ST_5/Rad_var/FR2_Rad_var_D_"$r".txt
	cd ..
	d=$c
	c=`expr $d + 1`
done

cd /data/atlastop3/cervato/qt/elaboration_ST_5/Rad_var

#######################################           Rad VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	for c in "" "_N" "_D"
	do
		mv "$b"_Rad_var"$c"_9.txt temp.txt 
		for r in 8 7 6 5 4 3 2 1 0
		do
			cat temp.txt "$b"_Rad_var"$c"_"$r".txt > file3.txt
			mv file3.txt temp.txt
		done
		mv temp.txt "$b"_RAD_VAR"$c"_"$i"_"$j"_"$z".txt 
	done 
done


echo "The end..."
