#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.

c=0




################################################ Fin VAR ############################ HIGHT STATA
mkdir Fin_var 
cd Fin_var/
i=-7
#j=0 #=default
z=4 
#z=4=default

mkdir Fin_var_Ini_-7_cos_4_HIGHSTAT
cd Fin_var_Ini_-7_cos_4_HIGHSTAT/
for j in -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10
do
	mkdir Fin_$j
	cd Fin_$j
	cp /afs/cern.ch/user/b/bcervato/parallel/ForBeatrice/run/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	#cp /backup_store/i-7_jvar_z4/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST_25/Plots_Fin_var_"$j"_HS.root
	cp EvtEff.txt ../../../../elaboration_ST_25/EvtEff_Fin_var_"$j"_HS.txt
	cp FR1.txt ../../../../elaboration_ST_25/FR1_Fin_var_"$j"_HS.txt
	cp FR2.txt ../../../../elaboration_ST_25/FR2_Fin_var_"$j"_HS.txt
	cp EvtEff_N.txt ../../../../elaboration_ST_25/EvtEff_Fin_var_N_"$j"_HS.txt
	cp FR1_N.txt ../../../../elaboration_ST_25/FR1_Fin_var_N_"$j"_HS.txt
	cp FR2_N.txt ../../../../elaboration_ST_25/FR2_Fin_var_N_"$j"_HS.txt
	cp EvtEff_D.txt ../../../../elaboration_ST_25/EvtEff_Fin_var_D_"$j"_HS.txt
	cp FR1_D.txt ../../../../elaboration_ST_25/FR1_Fin_var_D_"$j"_HS.txt
	cp FR2_D.txt ../../../../elaboration_ST_25/FR2_Fin_var_D_"$j"_HS.txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_25





<<COMMENT

################################ Ini VAR ###################################
mkdir Ini_var 
cd Ini_var/
#i=-7
j=0 #=default
z=4 
#z=4=default
mkdir Ini_var_Fin_"$j"_cos_"$z"
cd Ini_var_Fin_"$j"_cos_"$z"/
for i in -9 -8 -7 -6 -5 -4 -3 -2 -1 0
do
	mkdir Ini_$i
	cd Ini_$i
	cp /afs/cern.ch/user/b/bcervato/parallel/ForBeatrice/run/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST/Plots_Ini_var_"$i".root
	cp EvtEff.txt ../../../../elaboration_ST/EvtEff_Ini_var_"$i".txt
	cp FR1.txt ../../../../elaboration_ST/FR1_Ini_var_"$i".txt
	cp FR2.txt ../../../../elaboration_ST/FR2_Ini_var_"$i".txt
	cp EvtEff_N.txt ../../../../elaboration_ST/EvtEff_Ini_var_N_"$i".txt
	cp FR1_N.txt ../../../../elaboration_ST/FR1_Ini_var_N_"$i".txt
	cp FR2_N.txt ../../../../elaboration_ST/FR2_Ini_var_N_"$i".txt
	cp EvtEff_D.txt ../../../../elaboration_ST/EvtEff_Ini_var_D_"$i".txt
	cp FR1_D.txt ../../../../elaboration_ST/FR1_Ini_var_D_"$i".txt
	cp FR2_D.txt ../../../../elaboration_ST/FR2_Ini_var_D_"$i".txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST




################################################ Fin VAR #################################
mkdir Fin_var 
cd Fin_var/
i=-7
#j=0 #=default
z=4 
#z=4=default
mkdir Fin_var_Ini_-7_cos_4
cd Fin_var_Ini_-7_cos_4/
for j in -2 -1 0 1 2 3 4 5 6 7 
do
	mkdir Fin_$j
	cd Fin_$j
	cp /afs/cern.ch/user/b/bcervato/parallel/ForBeatrice/run/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST/Plots_Fin_var_"$j".root
	cp EvtEff.txt ../../../../elaboration_ST/EvtEff_Fin_var_"$j".txt
	cp FR1.txt ../../../../elaboration_ST/FR1_Fin_var_"$j".txt
	cp FR2.txt ../../../../elaboration_ST/FR2_Fin_var_"$j".txt
	cp EvtEff_N.txt ../../../../elaboration_ST/EvtEff_Fin_var_N_"$j".txt
	cp FR1_N.txt ../../../../elaboration_ST/FR1_Fin_var_N_"$j".txt
	cp FR2_N.txt ../../../../elaboration_ST/FR2_Fin_var_N_"$j".txt
	cp EvtEff_D.txt ../../../../elaboration_ST/EvtEff_Fin_var_D_"$j".txt
	cp FR1_D.txt ../../../../elaboration_ST/FR1_Fin_var_D_"$j".txt
	cp FR2_D.txt ../../../../elaboration_ST/FR2_Fin_var_D_"$j".txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST



################################### Cos VAR #####################
mkdir cos_var 
cd cos_var/
i=-7
j=0
mkdir Cos_var_Ini_"$i"_Fin_"$j"
cd Cos_var_Ini_"$i"_Fin_"$j"/
#z=4=default
for z in 0 1 2 3 4 5 6 7 8 9 
do
	mkdir cosSVPV_$z
	cd cosSVPV_$z
	cp /afs/cern.ch/user/b/bcervato/parallel/ForBeatrice/run/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST/Plots_cos_var_"$z".root
	cp EvtEff.txt ../../../../elaboration_ST/EvtEff_cos_var_"$z".txt
	cp FR1.txt ../../../../elaboration_ST/FR1_cos_var_"$z".txt
	cp FR2.txt ../../../../elaboration_ST/FR2_cos_var_"$z".txt
	cp EvtEff_N.txt ../../../../elaboration_ST/EvtEff_cos_var_N_"$z".txt
	cp FR1_N.txt ../../../../elaboration_ST/FR1_cos_var_N_"$z".txt
	cp FR2_N.txt ../../../../elaboration_ST/FR2_cos_var_N_"$z".txt
	cp EvtEff_D.txt ../../../../elaboration_ST/EvtEff_cos_var_D_"$z".txt
	cp FR1_D.txt ../../../../elaboration_ST/FR1_cos_var_D_"$z".txt
	cp FR2_D.txt ../../../../elaboration_ST/FR2_cos_var_D_"$z".txt
	
	cd ..
	d=$c
	c=`expr $d + 1`
done 
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST






################################################ Fin VAR LONG RANGE #################################
#mkdir Fin_var 
cd Fin_var/
i=-7
#j=0 #=default
z=4 
#z=4=default
#mkdir Fin_var_Ini_-7_cos_4
cd Fin_var_Ini_-7_cos_4/
for j in -2 -1 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17
do
#	mkdir Fin_$j
	cd Fin_$j
#	cp /afs/cern.ch/user/b/bcervato/parallel/ForBeatrice/run/trackExampleSV_$c.hist.root trackExampleSV.hist.root
#	cp ../../../*.sh .
#	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
#	bash Constructor_MakeClass.sh 
#	mv Eff_tuning1.C Eff_tuning.C
#	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST/Plots_Fin_var_"$j".root
	cp EvtEff.txt ../../../../elaboration_ST/EvtEff_Fin_var_"$j".txt
	cp FR1.txt ../../../../elaboration_ST/FR1_Fin_var_"$j".txt
	cp FR2.txt ../../../../elaboration_ST/FR2_Fin_var_"$j".txt
	cp EvtEff_N.txt ../../../../elaboration_ST/EvtEff_Fin_var_N_"$j".txt
	cp FR1_N.txt ../../../../elaboration_ST/FR1_Fin_var_N_"$j".txt
	cp FR2_N.txt ../../../../elaboration_ST/FR2_Fin_var_N_"$j".txt
	cp EvtEff_D.txt ../../../../elaboration_ST/EvtEff_Fin_var_D_"$j".txt
	cp FR1_D.txt ../../../../elaboration_ST/FR1_Fin_var_D_"$j".txt
	cp FR2_D.txt ../../../../elaboration_ST/FR2_Fin_var_D_"$j".txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST




#select the data that you want to consider in the analysis
#The code "elaboration.C" take into consideration just the EvtEff.txt, FR1.txt and FR2.txt. Thus make sure to build these three files with the "subfile" in which you are interessed.
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST
########################################           cos VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	mv "$b"_cos_var_0.txt temp.txt #cp "$b"_cos_var_-10.txt temp_"$j".txt
	for z in 1 2 3 4 5 6 7 8 9  
	do
		cat temp.txt "$b"_cos_var_"$z".txt > file3.txt
		mv file3.txt temp.txt
	done
	mv temp.txt "$b"_COS_VAR_HigStat_"$i"_"$j".txt 
done 
COMMENT
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_25
########################################           Fin VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	for c in "" "_N" "_D"
	do
		mv "$b"_Fin_var"$c"_-9_HS.txt temp.txt 
		for j in -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 
		do
			cat temp.txt "$b"_Fin_var"$c"_"$j"_HS.txt > file3.txt
			mv file3.txt temp.txt
		done
		mv temp.txt "$b"_FIN_VAR"$c"_"$i"_"$z"_HS.txt 
	done 
done

<<COMMENT
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST
########################################           Ini VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	mv "$b"_Ini_var_-9.txt temp.txt #cp "$b"_cos_var_-10.txt temp_"$j".txt
	for i in -8 -7 -6 -5 -4 -3 -2 -1 0  
	do
		cat temp.txt "$b"_Ini_var_"$i".txt > file3.txt
		mv file3.txt temp.txt
	done
	mv temp.txt "$b"_INI_VAR_"$j"_"$z".txt 
done 
COMMENT

echo "The end..."
