#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency estimation analysis
for i in "BB_direct" "ttbar"
do
	for j in "1_GeV" "2_GeV" "4_GeV"
    do
        cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/Efficiency_PtCut/$i/$j/
        echo "$i sample: Pt Cut = $j"
        bash script.sh
        echo "========================================================================================"
        echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        cp Plots.root ../../../elaboration/Plots_"$i"_"$j".root
	done
done
cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration/
root -q .x elaboration.C