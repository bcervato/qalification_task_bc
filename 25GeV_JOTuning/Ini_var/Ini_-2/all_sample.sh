#!/bin/sh
#Autor: Beatrice Cervato
#To run all the scripts for all the samples in just one command

<<COMMENT
for i in "" "15_" "25_" #"5_" "" "15_" "25_"
do
	cd "$i"JOTuning/
	cp ../5_JOTuning/Eff_tuning.C .
	bash script_JobOption_tuning.sh
	cd ..
done
COMMENT
cd elaboration_ST_5/
bash script_elaboration.sh
cd ../
for i in "" "_15" "_25" #"_5" "" "_15" "_25"
do
	cd elaboration_ST"$i"/
	cp ../elaboration_ST_5/script_elaboration.sh .
	cp ../elaboration_ST_5/Backup_correlation.C .
	cp ../elaboration_ST_5/script_elaboration_BK.sh .
	bash script_elaboration.sh
	cd ..
done
cd EC_comparison/
root -q .x PlotComp.C
