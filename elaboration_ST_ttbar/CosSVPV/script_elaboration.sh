#!/bin/sh
#Autor: Beatrice Cervato
#Changing some var. names to obtain the desidered analysis

#Please, copy the analysis that you whant to perform and paste it in the uncommented part of the script.
#REMEMBER to modify the value of j, i or z.....

echo "What analysis do you want to perform? Please, enter:"
echo "1 for cos variable, IniBDT = parameter"
echo "2 for cos variable, FinBDT = parameter"
echo "3 for FinBDT variable, cos = parameter"
echo "4 for FinBDT variable, IniBDT = parameter"
echo "5 for IniBDT variable, cos = parameter"
echo "6 for IniBDT variable, FinBDT = parameter"
echo 'mio'
#index = $(cat)
#read index

i=-8 ######################################## MODIFY #####################################
temp=1
t=12
j=0


#if ($index="4") #VAR = Cos ##########################new######################
cp Backup_elaboration.C elaboration.C
for b in "EvtEff" "FR1" "FR2"
do
	for wp in "tight" "medium" "loose"
	do
	    #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
	    #i = $(cat)
	    i=-8 ######################################## MODIFY #####################################
	    temp=1
	    t=12
	    j=0
	    sed -i 's/float index=0/float index=4/g' elaboration.C
	    sed -i 's/INPUT_FILE_1/'$b'_COS_VAR_'$wp'.txt/g' elaboration.C
	    sed -i 's/INPUT_FILE_2/'$b'_COS_VAR_N_'$wp'.txt/g' elaboration.C
	    sed -i 's/INPUT_FILE_3/'$b'_COS_VAR_D_'$wp'.txt/g' elaboration.C
	    sed -i 's/MESSAGE_LEG_1/'$wp' WP/g' elaboration.C

	    sed -i 's/Plots_1.root/Plots_Cos_var_1_'$wp'.root/g' elaboration.C
	    sed -i 's/MESSAGE_LEG_pt_1/Cos = 0.95/g' elaboration.C
	    
	    sed -i 's/Plots_2.root/Plots_Cos_var_10'$wp'.root/g' elaboration.C
	    sed -i 's/MESSAGE_LEG_pt_2/Cos = 0.50/g' elaboration.C

	    sed -i 's/Plots_3.root/Plots_Cos_var_19'$wp'.root/g' elaboration.C
	    sed -i 's/MESSAGE_LEG_pt_3/Cos = 0.05/g' elaboration.C
	    
	    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
	    sed -i 's/X_AXIS_LABEL/CosSVPV/g' elaboration.C
	    sed -i 's/SAVE_NAME/'$b'_Var-Cos_'$wp'/g' elaboration.C
	    sed -i 's/SAVE_NAME_EFF/'$b'_Var-Cos_EFF_'$wp'/g' elaboration.C
	    sed -i 's/S_N_pt/PtEff_COSVAR_'$wp'/g' elaboration.C
	    sed -i 's/myfile.root/EC_'$b'_'$wp'.root/g' elaboration.C

	    root -q .x elaboration.C
	    cp Backup_elaboration.C elaboration.C
	done
done
sed -i 's/float index=4/float index=0/g' elaboration.C

echo "Fine prima parte"
cp Backup_correlation.C correlation.C
for g in "FR1" "FR2"
do
	echo "Ciao"
    ##########################    FRs VS Eff    ####################
	i=-8 ######################################## MODIFY #####################################
    t=3
    j=0
    sed -i 's/INPUT_FILE_Y/'$g'_COS_VAR_'$wp'.txt/g' correlation.C
    sed -i 's/INPUT_FILE_X/EvtEff_COS_VAR_'$wp'.txt/g' correlation.C
    sed -i 's/MESSAGE_LEG_1/'$wp' WP/g' correlation.C
    sed -i 's/X_AXIS_LABEL/EvtEff/g' correlation.C
    sed -i 's/Y_AXIS_LABEL/1 - '$g'/g' correlation.C
    sed -i 's/SAVE_NAME/'$g'_Var-Cos_Correlation/g' correlation.C
    sed -i 's/ROOT_FILE_NAME/'$g'/g' correlation.C

    root -q .x correlation.C
    cp Backup_correlation.C correlation.C
done


<<COMMENT
echo "Fine prima parte"
cp Backup_correlation.C correlation.C
for g in "FR1" #"FR2"
do
	echo "Ciao"
    ##########################    FRs VS Eff    ####################
	z=4 ######################################## MODIFY #####################################
    t=3
    j=0
    sed -i 's/INPUT_FILE_Y/'$g'_COS_VAR_'$i'_'$j'.txt/g' correlation.C
    sed -i 's/INPUT_FILE_X/EvtEff_COS_VAR_'$i'_'$j'.txt/g' correlation.C
    sed -i 's/MESSAGE_LEG_1/COSBDTCut = var IniBDT = '$i'e-1 FinBDT = '$j'e-1 /g' correlation.C
    sed -i 's/X_AXIS_LABEL/EvtEff/g' correlation.C
    sed -i 's/Y_AXIS_LABEL/1 - '$g'/g' correlation.C
    sed -i 's/SAVE_NAME/'$g'_Var-Cos_Correlation/g' correlation.C
    sed -i 's/ROOT_FILE_NAME/'$g'/g' correlation.C

    root -q .x correlation.C
    cp Backup_correlation.C correlation.C
done
COMMENT


echo "The end"
