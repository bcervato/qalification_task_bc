#!/bin/sh
#Autor: Beatrice Cervato
#Changing some var. names to obtain the desidered analysis

#Please, copy the analysis that you whant to perform and paste it in the uncommented part of the script.
#REMEMBER to modify the value of j, i or z.....

echo "What analysis do you want to perform? Please, enter:"
echo "1 for cos variable, IniBDT = parameter"
echo "2 for cos variable, FinBDT = parameter"
echo "3 for FinBDT variable, cos = parameter"
echo "4 for FinBDT variable, IniBDT = parameter"
echo "5 for IniBDT variable, cos = parameter"
echo "6 for IniBDT variable, FinBDT = parameter"


#if ($index="4") #VAR = Fin ##########################new######################
cp Backup_elaboration.C elaboration.C
g++ estimator.C -o estimator
./estimator
for b in "EvtEff" "FR1" "FR2" "e1" "e2"
do
    #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    z=4 ######################################## MODIFY #####################################
    
    sed -i 's/INPUT_FILE_1/'$b'_SCATTER_INI_FIN_'$z'.txt/g' elaboration.C   
    sed -i 's/Z_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Scatter_Ini_Fin/g' elaboration.C
    if [ $b = "e1" ]
    then
    	sed -i 's/\/\/h3->SetMaximum(5)/h3->SetMaximum(25)/g' elaboration.C 
    fi

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
echo "The end"
