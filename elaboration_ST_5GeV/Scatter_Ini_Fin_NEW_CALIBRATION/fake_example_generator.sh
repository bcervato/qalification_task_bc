#!/bin/sh
#Autor: Beatrice Cervato
#Generation of a fake row data to test the code
for b in  "EvtEff" "FR1" "FR2"
do

	for j in -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10
	do
		for i in $(seq -9 $j);
		do
			echo $i >> "$b"_SCATTER_INI_FIN_4.txt 
		done
	done
done
