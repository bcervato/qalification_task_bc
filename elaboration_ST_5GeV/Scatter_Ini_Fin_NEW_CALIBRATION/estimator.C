#include <fstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>

using namespace std;
int main()
{

	double e=0, FR=0;
    ifstream fin_e("EvtEff_SCATTER_INI_FIN_4.txt"); 
    ifstream fin_FR("FR1_SCATTER_INI_FIN_4.txt"); 
    //ofstream e1("e1_SCATTER_INI_FIN_4.txt");
    ofstream e2("e2_SCATTER_INI_FIN_4.txt");
    ofstream e1;
    ofstream summary("Summary_table.txt");
    e1.open("e1_SCATTER_INI_FIN_4.txt");
  
	for (int count =0; count<209; count++)
	{
		fin_e>>e;
		fin_FR>>FR;
		e1<<e/FR<<endl;
		e2<<e-FR<<endl;	
	}
	fin_e.close();
	fin_FR.close();
	ifstream fin1_e("EvtEff_SCATTER_INI_FIN_4.txt"); 
    	ifstream fin1_FR("FR1_SCATTER_INI_FIN_4.txt"); 
	for (int j=-9; j<=10; j++)
	{
		for (int i=-9; i<j; i++)
		{
			fin1_e>>e;
			fin1_FR>>FR;
			summary<<i<<"	"<<j<<"	 "<<e<<"   "<<FR<<"    "<<e/FR<<endl;
		}
	}

	e1.close();
	e2.close();
	fin_e.close();
	fin_FR.close();
	fin1_e.close();
	fin1_FR.close();
	summary.close();

  return 0;
}
