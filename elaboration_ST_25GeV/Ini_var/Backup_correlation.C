#include <fstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TPDF.h"
#include "TMath.h"
#include "TLegend.h"

#include "TGedPatternSelect.h"
#include "TGColorSelect.h"
#include "TGColorDialog.h"
#include "TColor.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TSelectorDraw.h"
#include "TGTab.h"
#include "TGFrame.h"
#include "TGMsgBox.h"
#include "TClass.h"
#include "TLegend.h"
#include "TColor.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TEfficiency.h"

#include "THStack.h"
#include "TText.h"


using namespace std;
void correlation ()
{
	
	    #ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
	#endif

   SetAtlasStyle();
    gStyle->SetErrorX(0);
    TGraph *g_VAR1 = new TGraph();
	
	float x=0, y=0;
    int i=0;
    float index=0;
    
    ifstream finX("INPUT_FILE_X"); 
    ifstream finY("INPUT_FILE_Y"); 
    
	i=0;
	while (i<=19)
	{
		finX>>x;
		finY>>y;
		cout<<x<<endl;
		cout<<y<<endl;
		g_VAR1->SetPoint(i,x,y);
		i++;
	};  

    g_VAR1->SetMarkerColor(kOrange-3);
	g_VAR1->SetLineColor(0);
    
    TMultiGraph *mg = new TMultiGraph();

    mg->Add(g_VAR1,"cp");
    mg->GetXaxis()->SetTitle("X_AXIS_LABEL");
    mg->GetYaxis()->SetTitle("Y_AXIS_LABEL");
    
    TCanvas *cs = new TCanvas("cs","cs",10,10,1100,900);
    mg->Draw("a");
    TLegend *leg = new TLegend(); //0.12,0.75,0.38,0.9
    leg->SetBorderSize(0);
	leg->AddEntry(g_VAR1,"MESSAGE_LEG_1","P");
	leg->SetFillColor(kWhite);
	leg->Draw();
    cs->SaveAs("SAVE_NAME.png"); 
    
    TFile saving ("EC_EvtEff.root", "UPDATE");
    g_VAR1->GetXaxis()->SetLimits(0,0.77);
    g_VAR1->GetYaxis()->SetRangeUser(0,0.33);
    g_VAR1->SetName("ROOT_FILE_NAME");
    g_VAR1->Write();
    saving.Close();
    
    
  
  return 0;
}


