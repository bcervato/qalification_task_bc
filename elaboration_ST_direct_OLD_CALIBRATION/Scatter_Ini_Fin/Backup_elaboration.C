#include <fstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TPDF.h"
#include "TMath.h"
#include "TLegend.h"

#include "TGedPatternSelect.h"
#include "TGColorSelect.h"
#include "TGColorDialog.h"
#include "TColor.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TSelectorDraw.h"
#include "TGTab.h"
#include "TGFrame.h"
#include "TGMsgBox.h"
#include "TClass.h"
#include "TLegend.h"
#include "TColor.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TEfficiency.h"

#include "THStack.h"
#include "TText.h"


using namespace std;
void elaboration ()
{
	
	    #ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
	#endif

   SetAtlasStyle();
    gStyle->SetErrorX(0);
    
    TGraph2D *g_VAR1 = new TGraph2D();
	TH2F* h3 = new TH2F("h3", "2D analysis", 20, -0.95, 1.05, 20, -0.95, 1.05);
	
    
	//TH1F *hN = new TH1F("NUM", "Numerator", 21, -0.9, 1.2);
	//TH1F *hD = new TH1F("DENO", "Denominator", 21, -0.9, 1.2);

	double e=0, i=0, j=0;
    int count=0, c1=0, c2=0;
    float index=0;
    ifstream fin1("INPUT_FILE_1"); 
    //ifstream finN("INPUT_FILE_2"); 
   // ifstream finD("INPUT_FILE_3");  
	count=0;
	for (int count =0; count<209; count++)
	{
		fin1>>e;
		cout<<e<<endl;
		j=-0.9+0.1*c1;
		i=-0.9+0.1*c2;
		c2++;
		if (i<j){
			g_VAR1->SetPoint(count,j,i,e);
			//for (double a=0; a<=e; a+=0.0000001 )
				h3->Fill(j,i,e);
		}
		else{
			c1++;
			c2=0;
		}		
	}
	cout<<"++++++++++++++++++"<<endl;
	/*for (float j=-0.9; j<1; j+=0.1){		
		for (float i=-0.9; i<j; i+=0.1){
			fin1>>e;
			cout<<e<<endl;
			g_VAR1->SetPoint(count,j,i,e);
			cout<<e<<endl;
			count++;
		}
	}
	*/
	fin1.clear();
	//g_VAR1->SetMarkerColor(kOrange-3);
	g_VAR1->SetLineColor(1);
	g_VAR1->SetMarkerStyle(8);
	g_VAR1->SetMarkerSize(2);
	
    gStyle->SetPalette(kRainBow);
    g_VAR1->GetXaxis()->SetTitle("FinBDTCut");
    g_VAR1->GetYaxis()->SetTitle("IniBDTCut");
    g_VAR1->GetZaxis()->SetTitle("Z_AXIS_LABEL");
    h3->GetXaxis()->SetTitle("FinBDTCut");
    h3->GetYaxis()->SetTitle("IniBDTCut");
    h3->GetZaxis()->SetTitle("Z_AXIS_LABEL");    
      
    
    TCanvas *cs = new TCanvas("cs","cs");
    //cs->SetCanvasSize(2500,1500);
    //mg->Draw("PCOL");
    //g_VAR1->Draw("PCOL COLZ"); //To have a 3D scatter plot
     //g_VAR1->SetTitle("Graph title; X axis title; Y axis title; Z axis title");
     cs->cd(3)->SetRightMargin(0.18);
     //To set the z range:
    // g_VAR1->SetMinimum(0);
    //g_VAR1->SetMaximum(3);
     //g_VAR1->SetMinimum(0);
     //h3->SetMaximum(5);
    g_VAR1->SetTitle(" ; FinBDTCut; IniBDTCut; Z_AXIS_LABEL");
    //g_VAR1->Draw("CONT2 COLZ"); //To have a 2D binned plot, proiected on the x-y plane and with colors proportional to the z-axis
    h3->Draw("COLZ");
    
   /* TLegend *leg = new TLegend(); //0.12,0.75,0.38,0.9
    leg->SetBorderSize(0);
	leg->AddEntry(g_VAR1,"MESSAGE_LEG_1","P");
	leg->SetFillColor(kWhite);
	leg->Draw();
	*/
    cs->SaveAs("SAVE_NAME.png"); 
    
    
    
    TFile saving ("SAVE_NAME.root", "RECREATE");
    g_VAR1->GetXaxis()->SetLimits(-1,1);
    g_VAR1->GetYaxis()->SetRangeUser(-1,1);
    g_VAR1->SetName("ROOT_FILE_NAME");
    g_VAR1->Write();
    saving.Close();
	
  return 0;
}


