#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.

c=0

cd /data/atlastop3/cervato/qt/elaboration_ST_15GeV/
mkdir Fin_var
cd /data/atlastop3/cervato/qt/15GeV_JOTuning/



################################################ Fin VAR ############################ HIGHT STATA
mkdir Fin_var 
cd Fin_var/
i=-8
#j=0 #=default
z=4 
#z=4=default

mkdir Fin_var_Ini_-8_cos_4
cd Fin_var_Ini_-8_cos_4/
for j in -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10
do
	mkdir Fin_$j
	cd Fin_$j
	cp /nfs/morris_4/data/atlastop3/cervato/ForBeatrice/run/FinBDTCut_analysis/15GeV/$c/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST_15GeV/Fin_var/Plots_Fin_var_"$j".root
	cp EvtEff.txt ../../../../elaboration_ST_15GeV/Fin_var/EvtEff_Fin_var_"$j".txt
	cp FR1.txt ../../../../elaboration_ST_15GeV/Fin_var/FR1_Fin_var_"$j".txt
	cp FR2.txt ../../../../elaboration_ST_15GeV/Fin_var/FR2_Fin_var_"$j".txt
	cp EvtEff_N.txt ../../../../elaboration_ST_15GeV/Fin_var/EvtEff_Fin_var_N_"$j".txt
	cp FR1_N.txt ../../../../elaboration_ST_15GeV/Fin_var/FR1_Fin_var_N_"$j".txt
	cp FR2_N.txt ../../../../elaboration_ST_15GeV/Fin_var/FR2_Fin_var_N_"$j".txt
	cp EvtEff_D.txt ../../../../elaboration_ST_15GeV/Fin_var/EvtEff_Fin_var_D_"$j".txt
	cp FR1_D.txt ../../../../elaboration_ST_15GeV/Fin_var/FR1_Fin_var_D_"$j".txt
	cp FR2_D.txt ../../../../elaboration_ST_15GeV/Fin_var/FR2_Fin_var_D_"$j".txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 
cd /data/atlastop3/cervato/qt/elaboration_ST_15GeV/Fin_var/



########################################           Fin VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	for c in "" "_N" "_D"
	do
		mv "$b"_Fin_var"$c"_-9.txt temp.txt 
		for j in -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 
		do
			cat temp.txt "$b"_Fin_var"$c"_"$j".txt > file3.txt
			mv file3.txt temp.txt
		done
		mv temp.txt "$b"_FIN_VAR"$c"_"$i"_"$z".txt 
	done 
done

echo "The end..."
