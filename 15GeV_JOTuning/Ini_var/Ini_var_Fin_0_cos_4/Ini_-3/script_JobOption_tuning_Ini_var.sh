#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.

c=0



################################################ Ini VAR ############################ HIGHT STATA
mkdir Ini_var 
cd Ini_var/
j=0
#j=0 #=default
z=4 
#z=4=default


mkdir Ini_var_Fin_0_cos_4
cd Ini_var_Fin_0_cos_4
for i in -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10
do

	mkdir Ini_$i
	cd Ini_$i
	cp /nfs/morris_4/data/atlastop3/cervato/ForBeatrice/run/IniBDTCut_analysis/15GeV/$c/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_15GeV sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_ST_15GeV/Ini_var/Plots_Ini_var_"$i".root
	cp EvtEff.txt ../../../../elaboration_ST_15GeV/Ini_var/EvtEff_Ini_var_"$i".txt
	cp FR1.txt ../../../../elaboration_ST_15GeV/Ini_var/FR1_Ini_var_"$i".txt
	cp FR2.txt ../../../../elaboration_ST_15GeV/Ini_var/FR2_Ini_var_"$i".txt
	cp EvtEff_N.txt ../../../../elaboration_ST_15GeV/Ini_var/EvtEff_Ini_var_N_"$i".txt
	cp FR1_N.txt ../../../../elaboration_ST_15GeV/Ini_var/FR1_Ini_var_N_"$i".txt
	cp FR2_N.txt ../../../../elaboration_ST_15GeV/Ini_var/FR2_Ini_var_N_"$i".txt
	cp EvtEff_D.txt ../../../../elaboration_ST_15GeV/Ini_var/EvtEff_Ini_var_D_"$i".txt
	cp FR1_D.txt ../../../../elaboration_ST_15GeV/Ini_var/FR1_Ini_var_D_"$i".txt
	cp FR2_D.txt ../../../../elaboration_ST_15GeV/Ini_var/FR2_Ini_var_D_"$i".txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 

cd /data/atlastop3/cervato/qt/elaboration_ST_15GeV/Ini_var

#######################################           Ini VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	for c in "" "_N" "_D"
	do
		mv "$b"_Ini_var"$c"_-9.txt temp.txt 
		for i in -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 
		do
			cat temp.txt "$b"_Ini_var"$c"_"$i".txt > file3.txt
			mv file3.txt temp.txt
		done
		mv temp.txt "$b"_INI_VAR"$c"_"$j"_"$z".txt 
	done 
done


echo "The end..."
