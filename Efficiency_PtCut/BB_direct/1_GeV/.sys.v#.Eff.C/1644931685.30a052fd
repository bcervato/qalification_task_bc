#define Eff_cxx
#include "Eff.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <fstream>
#include <TFile.h>
#include <TColor.h>
#include <iostream>
#include <TImage.h>

void Eff::Loop()
{
if (fChain == 0) return;
	Long64_t nentries = fChain->GetEntriesFast();
	TH1::SetDefaultSumw2();
	//gStyle->SetLineStyle(1);
	
   TH1F * h_effEvnt=new TH1F("effEvnt","Efficiency per event",22,0.,1.1);
   TH1F * h_bPtAll =new TH1F("bPtAll","All Bhadron Pt",200,0.,1.e5);
   TH1F * h_bPtMat =new TH1F("bPtMat","Matched Bhadron Pt",200,0.,1.e5);
   TH1F * h_bPtEff =new TH1F("bPtEff","Bhadron Efficiency vs Pt",200,0.,1.e5);
   TH1F * h_bPtvsSVPt =new TH1F("bPtvsSVPt","Bhadron Pt minus SV Pt",100,-1.e4,4.e4);
   
   TH1F * h_bEtaAll =new TH1F("bEtaAll","All Bhadron Eta",50,-4.,4.);
   TH1F * h_bEtaMat =new TH1F("bEtaMat","Matched Bhadron Eta",50,-4.,4.);
   TH1F * h_bEtaEff =new TH1F("bEtaEff","Bhadron Efficiency vs Eta",50,-4.,4.);
   
   TH1F * h_bPtAll_rid =new TH1F("bPtAll_rid","All Bhadron Pt",50,0.,20.e3);
   TH1F * h_bPtMat_rid =new TH1F("bPtMat_rid","Matched Bhadron Pt",50,0.,20.e3);
   TH1F * h_bPtEff_rid =new TH1F("bPtEff_rid","Bhadron Efficiency vs Pt",50,0.,20.e3);
       
   TH1F * h_NumberBHNonMatch1Evnt =new TH1F("NumberBHNonMatch1Evnt","Number of non-match BH in une event",50,0,7);
       
   TH1F * rebin = new TH1F("rebin","dRVrtBH",100,0.,0.25);

   h_bPtEff->GetXaxis()->SetTitle("BH Pt [MeV]");
   h_bPtEff->GetYaxis()->SetTitle("Efficiency");
   
   h_bPtEff_rid->GetXaxis()->SetTitle("BH Pt [MeV]");
   h_bPtEff_rid->GetYaxis()->SetTitle("Efficiency");
   
   h_bEtaEff->GetXaxis()->SetTitle("BH Eta");
   h_bEtaEff->GetYaxis()->SetTitle("Efficiency");
   
   h_effEvnt->GetXaxis()->SetTitle("Event Efficiency");
   h_effEvnt->GetYaxis()->SetTitle("Counts");
   
   Long64_t nbytes = 0, nb = 0;
    
   float OutPtCut_counter=0, InPtCut_counter=0;
   int nEventsWithMatchedSV=0; //Number of events with at least one SV matched to b-hadron
   int NonAcc=0, NonAccTot=0, count_non_reco=0, out_eta=0, event_non_reco=0;
   ofstream fout("Parameter.txt");
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if(OutPtCut<=2)
      	OutPtCut_counter++;
      else
      	InPtCut_counter++;

      if(nVrt==0)continue;   //Skip events without reconstructed SVs

      int nMatchedSV=0;   // Number of matched SVs in the given event
      NonAcc=0;
      for(int iv=0; iv<nVrt; iv++){
		if(fabs(BHadEta[matchVrtBH[iv]]>2.5)){
			NonAcc++;
			NonAccTot++;
			//std::cout<<"BH outside acceptance, # in this ("<<jentry<<") event = "<<NonAcc<<std::endl;
		}
		else{
	      	if(dRVrtBH[iv]>0.2) continue;      // Effectively no matching	
			nMatchedSV++;                      // count number of SVs matched to a b-hadron
			h_bPtMat->Fill(BHadPt[matchVrtBH[iv]],1.);  // Pt of the matched b-hadron for the given SV
			h_bPtMat_rid->Fill(BHadPt[matchVrtBH[iv]],1.);
			h_bPtvsSVPt->Fill(BHadPt[matchVrtBH[iv]]-VrtPt[iv],1.);  // Pt of matched b-hadron minus Pt of SV
			h_bEtaMat->Fill(BHadEta[matchVrtBH[iv]],1.);  	
		}
      }
      h_effEvnt->Fill(float(nMatchedSV)/(float)(nBHad-NonAcc),1.);  //Fill event based b-hadron detection efficiency histogram
      if(nMatchedSV)nEventsWithMatchedSV++;   //Number of events with at least one SV matched to b-hadron

      for(int ib=0; ib<nBHad; ib++){
           if(fabs(BHadEta[ib]>2.5)){
			   out_eta++;
			   continue;
			   }
			h_bPtAll->Fill(BHadPt[ib],1.);  // Fill pt distribution of ALL b-hadrons
			h_bPtAll_rid->Fill(BHadPt[ib],1.);
            h_bEtaAll->Fill(BHadEta[ib],1.);
      }
      
      if(nBHad > nVrt)
      {
		count_non_reco+=(nBHad-nVrt);
		event_non_reco++;
		h_NumberBHNonMatch1Evnt->Fill((float)(nBHad-nVrt),1.);
      }

    }  //--End of event loop
	h_bPtMat->SetMarkerStyle(3);
	h_bPtMat->SetFillColor(kRed);
    h_bPtEff->Divide(h_bPtMat, h_bPtAll, 1.,1.,"O"); // Calculate ratio of histograms - pt-dependent B-hadron efficiency
    h_bPtEff_rid->Divide(h_bPtMat_rid, h_bPtAll_rid, 1.,1.,"O");
    h_bEtaEff->Divide(h_bEtaMat, h_bEtaAll, 1.,1.,"");
    TH1F * h_EtaNonRec = (TH1F*) h_bEtaAll->Clone("EtaNonRec");
    TH1F * h_PtNonRec = (TH1F*) h_bPtAll->Clone("PtNonRec");   
    h_PtNonRec->SetTitle("Non reconstructed BH pt");
    h_EtaNonRec->SetTitle("Non reconstructed BH Eta");
    //h_bPtEff->SetBinContent(h_bPtEff->GetNbinsX(), h_bPtEff->GetBinContent(h_bPtEff->GetNbinsX()) + h_bPtEff->GetBinContent(h_bPtEff->GetNbinsX() + 1));
    h_EtaNonRec->Add(h_bEtaMat, -1.);
    h_PtNonRec->Add(h_bPtMat, -1.);
    std::cout<<" Overall B-hadron detection efficiency per event ="<<(float)nEventsWithMatchedSV/((float)(nentries)-OutPtCut_counter)<<'\n';
   // std::cout<<" # of events in which some BH has not produced a jet = "<<event_non_reco<<" over a total number of events = "<<nentries<<" That should be "<<OutPtCut_counter<<" + "<<InPtCut_counter<<" = "<<OutPtCut_counter+InPtCut_counter<<'\n';
   // std::cout<<" # of BH in excess of the secondary verices = "<<count_non_reco<<std::endl;
   // std::cout<<" Total # of most close to SV BH outside tracker acceptance  = "<<NonAccTot<<". Total number of BH outside the tracker acceptance = "<<out_eta<<'\n';
    
    fout<<" Overall B-hadron detection efficiency per event ="<<(float)nEventsWithMatchedSV/((float)(nentries)-OutPtCut_counter)<<'\n';
    fout<<" # of events in which some BH has not produced a jet = "<<event_non_reco<<" over a total number of events = "<<nentries<<" That should be "<<OutPtCut_counter<<" + "<<InPtCut_counter<<" = "<<OutPtCut_counter+InPtCut_counter<<'\n';
    fout<<" # of BH in excess of the secondary verices = "<<count_non_reco<<std::endl;
    fout<<" Total # of most close to SV BH outside tracker acceptance  = "<<NonAccTot<<". Total number of BH outside the tracker acceptance = "<<out_eta<<'\n';
   
    //h_effEvnt->SetFillColor(kGreen);
   //char path [100]="/nfs/munch_3/home/cervato/QT/4_results_20-01-22/BB_Direct/";
   TCanvas *c = new TCanvas;
   //h_effEvnt->SetFillColor(kGreen);
   h_effEvnt->Draw("HIST");
   TImage *img = TImage::Create();
   img->FromPad(c);
   img->WriteImage("effEvnt.jpg");
   delete(c);
      
   TCanvas *c1 = new TCanvas;   
   h_bPtAll->Draw("HIST");   
   img->FromPad(c1);
   img->WriteImage("bPtAll.jpg");
    
      TCanvas *c2 = new TCanvas;
   h_bPtEff->Draw("HIST");
   img->FromPad(c2);
   img->WriteImage("bPtEff.jpg");
   delete(c2);
   
      TCanvas *c3 = new TCanvas;
   h_bPtMat->Draw("HIST");
   img->FromPad(c3);
   img->WriteImage("bPtMat.jpg");
   delete(c3); 
   
      TCanvas *c4 = new TCanvas;
   h_bPtvsSVPt->Draw("HIST");
   img->FromPad(c4);
   img->WriteImage("bPtvsSVPt.jpg");
   delete(c4);
   
   TCanvas *c5 = new TCanvas;
   h_bEtaEff->Draw("HIST");
   img->FromPad(c5);
   img->WriteImage("bEtaEff.jpg");
   delete(c5);
   
   TCanvas *c6 = new TCanvas;
   h_bEtaMat->Draw("HIST");
   img->FromPad(c6);
   img->WriteImage("bEtaMat.jpg");
   delete(c6);
   
      TCanvas *c7 = new TCanvas;
   h_EtaNonRec->Draw("HIST");
   img->FromPad(c7);
   img->WriteImage("EtaNonRec.jpg");
   delete(c7);

      TCanvas *c8 = new TCanvas;
   h_PtNonRec->Draw("HIST");
   img->FromPad(c8);
   img->WriteImage("PtNonRec.jpg");
   delete(c8);
   
   TCanvas *c9 = new TCanvas;
   h_bPtEff_rid->Draw("HIST");
   img->FromPad(c9);
   img->WriteImage("bPtEff_rec.jpg");
   delete(c9);
   
   TFile saving ("Plots.root","recreate");
   //h_effEvnt->SetFillColor(kRed);
   h_effEvnt->Draw();
   h_effEvnt->SetOption("HIST");
   
   h_bPtAll->Draw();
   h_bPtAll->SetOption("HIST");
   h_bPtAll ->Write();
   
   h_bPtMat->Draw();
   h_bPtMat->SetOption("HIST");
   h_bPtMat->Write();
   
   h_bPtEff->Draw();
   h_bPtEff->SetOption("HIST");
   h_bPtEff->Write(); 
   
   h_bPtvsSVPt->Draw();
   h_bPtvsSVPt->SetOption("HIST");
   h_bPtvsSVPt->Write(); 
   
   h_bEtaAll->Draw();
   h_bEtaAll->SetOption("HIST");
   h_bEtaAll->Write(); 
   
   h_bEtaMat->Draw();
   h_bEtaMat->SetOption("HIST");
   h_bEtaMat->Write(); 
   
   h_bEtaEff->Draw();
   h_bEtaEff->SetOption("HISTO");
   h_bEtaEff->Write();
   
   h_EtaNonRec->Draw();
   h_EtaNonRec->SetOption("HIST");
   h_EtaNonRec->Write();

   h_PtNonRec->Draw();
   h_PtNonRec->SetOption("HIST");
   h_PtNonRec->Write();

   h_bPtEff_rid->Draw();
   h_bPtEff_rid->SetOption("HIST");
   h_bPtEff_rid->Write();
   
   saving.Close();
   
}