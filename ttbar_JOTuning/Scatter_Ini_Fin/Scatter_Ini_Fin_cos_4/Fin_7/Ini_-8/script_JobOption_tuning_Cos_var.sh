#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.

c=0


cd /data/atlastop3/cervato/qt/elaboration_ST_ttbar/
mkdir Cos_var
cd /data/atlastop3/cervato/qt/ttbar_JOTuning/

################################################ Cos VAR ############################ HIGHT STATA
mkdir Cos_var 
cd Cos_var/
j=0
#j=0 #=default
i=-8
#z=4=default


mkdir Cos_var_WP
cd Cos_var_WP
for wp in "tight" "medium" "loose"
do
	mkdir $wp
	cd $wp
	for z in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
	do

		mkdir Cos_$z
		cd Cos_$z
		cp /nfs/morris_4/data/atlastop3/cervato/ForBeatrice/run/CosSVPV_analysis/ttbar/$c/trackExampleSV_$c.hist.root trackExampleSV.hist.root
		cp ../../../../*.sh .
		cp ../../../../Eff_tuning.C ./Eff_tuning1.C
		echo "=============WP = $wp, cosSVPVCut = $z -------- $c iteration============"
		bash Constructor_MakeClass.sh 
		mv Eff_tuning1.C Eff_tuning.C
		bash script_MakeClass.sh

		echo "========================================================================================"
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		cp Plots.root ../../../../../elaboration_ST_ttbar/Cos_var/Plots_Cos_var_"$z"_"$wp".root
		cp EvtEff.txt ../../../../../elaboration_ST_ttbar/Cos_var/EvtEff_Cos_var_"$z"_"$wp".txt
		cp FR1.txt ../../../../../elaboration_ST_ttbar/Cos_var/FR1_Cos_var_"$z"_"$wp".txt
		cp FR2.txt ../../../../../elaboration_ST_ttbar/Cos_var/FR2_Cos_var_"$z"_"$wp".txt
		cp EvtEff_N.txt ../../../../../elaboration_ST_ttbar/Cos_var/EvtEff_Cos_var_N_"$z"_"$wp".txt
		cp FR1_N.txt ../../../../../elaboration_ST_ttbar/Cos_var/FR1_Cos_var_N_"$z"_"$wp".txt
		cp FR2_N.txt ../../../../../elaboration_ST_ttbar/Cos_var/FR2_Cos_var_N_"$z"_"$wp".txt
		cp EvtEff_D.txt ../../../../../elaboration_ST_ttbar/Cos_var/EvtEff_Cos_var_D_"$z"_"$wp".txt
		cp FR1_D.txt ../../../../../elaboration_ST_ttbar/Cos_var/FR1_Cos_var_D_"$z"_"$wp".txt
		cp FR2_D.txt ../../../../../elaboration_ST_ttbar/Cos_var/FR2_Cos_var_D_"$z"_"$wp".txt
		cd ..
		d=$c
		c=`expr $d + 1`
	done
	cd ..
done
  


cd /data/atlastop3/cervato/qt/elaboration_ST_ttbar/Cos_var

#######################################           Cos VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
	for c in "" "_N" "_D"
	do
		for wp in "tight" "medium" "loose"
		do
			mv "$b"_Cos_var"$c"_19_"$wp".txt temp.txt 
			for z in 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
			do
				cat temp.txt "$b"_Cos_var"$c"_"$z"_"$wp".txt > file3.txt
				mv file3.txt temp.txt
			done
			mv temp.txt "$b"_COS_VAR"$c"_"$wp".txt 
		done
	done 
done


echo "The end..."
