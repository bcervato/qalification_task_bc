#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.



Run_Root() {
	cp script_MakeClass.sh Constructor_MakeClass.sh Setting_tuning/"$1"/IniBDT_"$2"/FinBDT_"$3"/cosSVPV_"$4"/
	cd Setting_tuning/
	cp Eff_tuning.C "$1"/IniBDT_"$2"/FinBDT_"$3"/cosSVPV_"$4"/Eff_tuning1.C
	cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/Setting_tuning/"$1"/IniBDT_"$2"/FinBDT_"$3"/cosSVPV_"$4"/
	echo "$1 sample: v2tIniBDTCut = $2, v2tFinBDTCut = $3, cosSVPVCut = $4"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../../elaboration_setting_tuning/Plots_"$1"_"$2"_"$3"_"$4".root
	cp EvtEff.txt ../../../../../elaboration_setting_tuning/EvtEff_"$1"_"$2"_"$3"_"$4".txt
	cp FR1.txt ../../../../../elaboration_setting_tuning/FR1_"$1"_"$2"_"$3"_"$4".txt
	cp FR2.txt ../../../../../elaboration_setting_tuning/FR2_"$1"_"$2"_"$3"_"$4".txt
	cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/
}



for a in "BB_direct" ##################"ttbar"
do
	for i in -8 #-10 -8 -6 -4 -2 
    do
        for j in -2 0 2 4 6 
        do
            for z in -10 -6 -2 2 6 
            do
				Run_Root $a $i $j $z 
            done
        done
	done
done

cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_setting_tuning/


#select the data that you want to consider in the analysis
#The code "elaboration.C" take into consideration just the EvtEff.txt, FR1.txt and FR2.txt. Thus make sure to build these three files with the "subfile" in which you are interessed.

########################################           cos VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
    for i in -8 #-10 -8 -6 -4 -2
    do
        for j in -2 0 2 4 6 
        do
            mv "$b"_BB_direct_"$i"_"$j"_-10.txt temp_"$j".txt
            for z in -6 -2 2 6 
            do
                cat temp_"$j".txt "$b"_BB_direct_"$i"_"$j"_"$z".txt > file3.txt
                mv file3.txt temp_"$j".txt
                mv  "$b"_BB_direct_"$i"_"$j"_"$z".txt trash/
            done
            mv temp_"$j".txt "$b"_BB_direct_"$i"_"$j"_COS_VAR.txt 
        done
    done
done

########################################           FinBDT VARIABLE          ##########################################

for b in "EvtEff" "FR1" "FR2"
do
    for i in -8 #-10 -8 -6 -4 -2
    do
        for z in -10 -6 -2 2 6
        do
            cp "$b"_BB_direct_"$i"_-2_"$z".txt temp_"$z".txt
            for j in 0 2 4 6 
            do
                cat temp_"$z".txt "$b"_BB_direct_"$i"_"$j"_"$z".txt > file3.txt
                mv file3.txt temp_"$z".txt
                mv  "$b"_BB_direct_"$i"_"$j"_"$z".txt trash/
            done
            mv temp_"$z".txt "$b"_BB_direct_"$i"_"$z"_Fin_VAR.txt 
        done
    done
done

<<COMMENT
########################################           IniBDT VARIABLE          ##########################################

for b in "EvtEff" "FR1" "FR2"
do
    for j in -2 0 2 4 6 
    do
        for z in -10 -6 -2 2 6
        do
            cp "$b"_BB_direct_-10_"$j"_"$z".txt temp_"$z".txt
            for i in -10 -8 -6 -4 -2 
            do
                cat temp_"$z".txt "$b"_BB_direct_"$i"_"$j"_"$z".txt > file3.txt
                mv file3.txt temp_"$z".txt
                mv  "$b"_BB_direct_"$i"_"$j"_"$z".txt trash/
            done
            mv temp_"$z".txt "$b"_BB_direct_"$j"_"$z"_Ini_VAR.txt 
        done
    done
done
COMMENT
#bash script_elaboration.sh

