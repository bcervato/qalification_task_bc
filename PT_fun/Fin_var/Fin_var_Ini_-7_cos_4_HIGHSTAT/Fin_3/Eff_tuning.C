#define Eff_tuning_cxx
#include "Eff_tuning.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <fstream>
#include <TFile.h>
#include <TColor.h>
#include <iostream>
#include <TImage.h>
#include <TEfficiency.h>

void Eff_tuning::Loop()
{
if (fChain == 0) return;
	Long64_t nentries = fChain->GetEntriesFast();
	
   TH1F * h_bPtAll_1 =new TH1F("bPtAll_1","All Bhadron Pt",1,2e3,10e3);
   TH1F * h_bPtMat_1 =new TH1F("bPtMat_1","Matched Bhadron Pt",1,2e3,10e3);
   
   TH1F * h_bPtAll_2 =new TH1F("bPtAll_2","All Bhadron Pt",2,10e3,20e3);
   TH1F * h_bPtMat_2 =new TH1F("bPtMat_2","Matched Bhadron Pt",2,10e3,20e3);
   
   TH1F * h_bPtAll_3 =new TH1F("bPtAll_3","All Bhadron Pt",1,20e3,100e3);
   TH1F * h_bPtMat_3 =new TH1F("bPtMat_3","Matched Bhadron Pt",1,20e3,100e3);
  
   //auto h_bPtAll = new TH1F();
   //auto h_bPtMat = new TH1F(); 
   TH1F * h_bPtEff =new TH1F("bPtEff","Bhadron Efficiency vs Pt",6,0,3e4);
    
   h_bPtEff->GetXaxis()->SetTitle("BH Pt [MeV]");
   h_bPtEff->GetYaxis()->SetTitle("Efficiency");
   
   Long64_t nbytes = 0, nb = 0;   

   float SV_bad_2=0, nVrt_2=0, Sum_VrtTrkHF=0, SV_bad=0, SV_bad_tot=0, SV_good_tot=0, SV_good=0, Evt_all_fake=0, BHtot=0, NOVrt=0, out_eta=0, OutPtCut_counter=0, nVrtTOT=0, VrtTrk0=0, VrtTrk0TOT=0, nonMatchedSVTOT=0, Evnt_BHless=0, Evnt_BHless_temp=0, VrtTrk_non_0=0;
   int nEventsWithMatchedSV=0; //Number of events with at least one SV matched to b-hadron
   int NonAcc=0, NonAccTot=0, count_non_reco=0, event_non_reco=0;
   float SV_bad_tot_10=0;
   float SV_bad_tot_15=0;
   float SV_bad_tot_20=0;
   float SV_bad_tot_25=0;
   float nVrtTOT_10=0;
   float nVrtTOT_20=0;
   float nVrtTOT_25=0;
   float nVrtTOT_15=0;
   ofstream fout2N_10("FR2_N_10.txt");
   ofstream fout2D_10("FR2_D_10.txt");
   ofstream fout2N_15("FR2_N_15.txt");
   ofstream fout2D_15("FR2_D_15.txt");
   ofstream fout2N_20("FR2_N_20.txt");
   ofstream fout2D_20("FR2_D_20.txt");
   ofstream fout2N_25("FR2_N_25.txt");
   ofstream fout2D_25("FR2_D_25.txt");
   
	float SV_bad_2_10=0;
	float SV_bad_2_15=0;
	float SV_bad_2_20=0;
	float SV_bad_2_25=0;
	float nVrt_2_10=0;
	float nVrt_2_15=0;
	float nVrt_2_20=0;
	float nVrt_2_25=0;
	float Sum_10_VrtTrkHF=0;
	float Sum_20_VrtTrkHF=0;
	float Sum_15_VrtTrkHF=0;
	float Sum_25_VrtTrkHF=0;
      
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      
      VrtTrk0=0;
      SV_good=0;
      SV_bad_2_10=0;
      SV_bad_2_15=0;
      SV_bad_2_20=0;
      SV_bad_2_25=0;
      nVrt_2_10=0;
      nVrt_2_15=0;
      nVrt_2_20=0;
      nVrt_2_25=0;
      Sum_10_VrtTrkHF=0;
      Sum_20_VrtTrkHF=0;
      Sum_15_VrtTrkHF=0;
      Sum_25_VrtTrkHF=0;
      for(int iv=0; iv<nVrt; iv++){
		  
		if (BHadPt[matchVrtBH[iv]]<10000)
			Sum_10_VrtTrkHF+=VrtTrkHF[iv];
		if (BHadPt[matchVrtBH[iv]]>10000 && BHadPt[matchVrtBH[iv]]<15000)
			Sum_15_VrtTrkHF+=VrtTrkHF[iv];
		if (BHadPt[matchVrtBH[iv]]>15000 && BHadPt[matchVrtBH[iv]]<20000)
			Sum_20_VrtTrkHF+=VrtTrkHF[iv];
		if (BHadPt[matchVrtBH[iv]]>20000 )
			Sum_25_VrtTrkHF+=VrtTrkHF[iv];
		  
		if (dRVrtBH[iv]<0.6 && VrtTrkHF[iv]>0 && fabs(BHadEta[matchVrtBH[iv]])<2.5)
		{
			SV_good++;			        			    // count number of SVs matched to a b-hadron
			h_bPtMat_1->Fill(BHadPt[matchVrtBH[iv]],1.);  // Pt of the matched b-hadron for the given SV
			h_bPtMat_2->Fill(BHadPt[matchVrtBH[iv]],1.);
			h_bPtMat_3->Fill(BHadPt[matchVrtBH[iv]],1.);
		}	
		if (VrtTrkHF[iv]>0 && BHadPt[matchVrtBH[iv]]<10000)	
			nVrt_2_10++;
		if (VrtTrkHF[iv]>0 && BHadPt[matchVrtBH[iv]]>10000 && BHadPt[matchVrtBH[iv]]<15000)
			nVrt_2_15++;
		if (VrtTrkHF[iv]>0 && BHadPt[matchVrtBH[iv]]>15000 && BHadPt[matchVrtBH[iv]]<20000)
			nVrt_2_20++;
		if (VrtTrkHF[iv]>0 && BHadPt[matchVrtBH[iv]]>20000 )
			nVrt_2_25++;
		
		if (VrtTrkHF[iv]==0 && BHadPt[matchVrtBH[iv]]<10000)
			SV_bad_2_10++;
		if (VrtTrkHF[iv]==0 && BHadPt[matchVrtBH[iv]]>10000 && BHadPt[matchVrtBH[iv]]<15000)
			SV_bad_2_15++;
		if (VrtTrkHF[iv]==0 && BHadPt[matchVrtBH[iv]]>15000 && BHadPt[matchVrtBH[iv]]<20000)
			SV_bad_2_20++;
		if (VrtTrkHF[iv]==0 && BHadPt[matchVrtBH[iv]]>20000 )
			SV_bad_2_25++;
			
      }
     
      if((nVrt_2_10 + SV_bad_2_10)>0 && Sum_10_VrtTrkHF>0){
      	SV_bad_tot_10+=SV_bad_2_10;  //Numerators of FR2
      	nVrtTOT_10+=nVrt_2_10; 		 //Denominators of FR2
      	}
      if((nVrt_2_15 + SV_bad_2_15)>0 && Sum_15_VrtTrkHF>0){
      	SV_bad_tot_15+=SV_bad_2_15; 
      	nVrtTOT_15+=nVrt_2_15;
		}
      if((nVrt_2_20 + SV_bad_2_20)>0 && Sum_20_VrtTrkHF>0){
      	SV_bad_tot_20+=SV_bad_2_20; 
      	nVrtTOT_20+=nVrt_2_20;
		}
      if((nVrt_2_25 + SV_bad_2_25)>0 && Sum_25_VrtTrkHF>0){
      	SV_bad_tot_25+=SV_bad_2_25; 
      	nVrtTOT_25+=nVrt_2_25;
		}
	 
      if(SV_good>0)
          nEventsWithMatchedSV++;   //Number of events with at least one SV matched to b-hadron = Numerator of Evt_eff

      if(nBHad==0) 
		Evnt_BHless++;
      else
      {
		  Evnt_BHless_temp=0;
		  for(int ib=0; ib<nBHad; ib++){
			  if (fabs(BHadEta[ib])<2.5 && BHadPt[ib]>2000){
				  h_bPtAll_1->Fill(BHadPt[ib],1.);  // Fill pt distribution of ALL b-hadrons
				  h_bPtAll_2->Fill(BHadPt[ib],1.); 
				  h_bPtAll_3->Fill(BHadPt[ib],1.); 
				  
				  BHtot++; //Denominator of BH-eff
			  }
			  else
				  Evnt_BHless_temp++; 
			}
			if (Evnt_BHless_temp==nBHad)
				Evnt_BHless++;
      }
    }  //--End of event loop
    
    
    std::cout<<" Eff_BH = (number of good SV) / (number of BH with pt and eta constraint) = "<<(float)(SV_good_tot)/(float)(BHtot)<<'\n';
    std::cout<<" Fake Rate (2) = (Total number of bad SV in events with at least 1 good SV)/(total number of SV in events with at least 1 good SV) = "<<(float)SV_bad_tot_10/((float)(nVrtTOT_10))<<'\n';
//    fout2<<(float)SV_bad_tot/((float)(nVrtTOT))<<'\n';
		
	fout2N_10<<SV_bad_tot_10<<'\n';
	fout2D_10<<nVrtTOT_10<<'\n';
	fout2N_15<<SV_bad_tot_15<<'\n';
	fout2D_15<<nVrtTOT_15<<'\n';
	fout2N_20<<SV_bad_tot_20<<'\n';
	fout2D_20<<nVrtTOT_20<<'\n';
	fout2N_25<<SV_bad_tot_25<<'\n';
	fout2D_25<<nVrtTOT_25<<'\n';
	
	/*
	h_bPtMat->Add(h_bPtMat_1);
	h_bPtMat->Add(h_bPtMat_2);
	h_bPtMat->Add(h_bPtMat_3);
	h_bPtAll->Add(h_bPtAll_1);
	h_bPtAll->Add(h_bPtAll_2);
	h_bPtAll->Add(h_bPtAll_3);
	
	
	TList *list = new TList;
	list->Add(h_bPtMat_1);
	list->Add(h_bPtMat_2);
	list->Add(h_bPtMat_3);
	TH1F *h_bPtMat = (TH1F*)h_bPtMat_1->Clone("bPtMat");
	h_bPtMat->Reset();
	h_bPtMat->Merge(list, "NOL");
	
	TList *list2 = new TList;
	list2->Add(h_bPtAll_1);
	list2->Add(h_bPtAll_2);
	list2->Add(h_bPtAll_3);
	TH1F *h_bPtAll = (TH1F*)h_bPtAll_1->Clone("bPtAll");
	h_bPtAll->Reset();
	h_bPtAll->Merge(list2);
*/
	
	
	TEfficiency* pEff_1 =0;
	TEfficiency* pEff_2 =0;
	TEfficiency* pEff_3 =0;
	//TEfficiency* pEff = new TEfficiency("bPtAll_clone","Reco-efficiency;BH Pt [MeV];#epsilon",50,0.,2.e4,100,0,0.5);
	TFile saving ("Plots.root","RECREATE");

	if(TEfficiency::CheckConsistency(*h_bPtMat_1,*h_bPtAll_1))
	{ 
	  pEff_1 = new TEfficiency(*h_bPtMat_1,*h_bPtAll_1);
	  pEff_1->Write();
	}  

	if(TEfficiency::CheckConsistency(*h_bPtMat_2,*h_bPtAll_2))
	{ 
	  pEff_2 = new TEfficiency(*h_bPtMat_2,*h_bPtAll_2);
	  pEff_2->Write();
	}  

	if(TEfficiency::CheckConsistency(*h_bPtMat_3,*h_bPtAll_3))
	{ 
	  pEff_3 = new TEfficiency(*h_bPtMat_3,*h_bPtAll_3);
	  pEff_3->Write();
	}  
	
	pEff_1->Draw();
	pEff_2->Draw();
	pEff_3->Draw();
	

   //h_bPtEff->Draw();
   h_bPtEff->SetOption();
   h_bPtEff->Write(); 
   //h_bPtMat->Write();
   //h_bPtAll->Write();

 
   saving.Close();
}
