#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.



c=0

################################################ Fin VAR ############################ HIGHT STATA
mkdir Fin_var 
cd Fin_var/
i=-7
#j=0 #=default
z=4 
#z=4=default

mkdir Fin_var_Ini_-7_cos_4_HIGHSTAT
cd Fin_var_Ini_-7_cos_4_HIGHSTAT/

for j in -9 -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10
do
	mkdir Fin_$j
	cd Fin_$j
	cp /data/atlastop3/cervato/ForBeatrice/run/OUTPUT/Fin_var_all_sample/trackExampleSV_$c.hist.root trackExampleSV.hist.root
	cp ../../../*.sh .
	cp ../../../Eff_tuning.C ./Eff_tuning1.C
	echo "BB_direct sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../elaboration_Pt/Plots_Fin_var_"$j"_HS.root

	cp FR2_N_10.txt ../../../../elaboration_Pt/FR2_Fin_var_N_"$j"_10.txt
	cp FR2_D_10.txt ../../../../elaboration_Pt/FR2_Fin_var_D_"$j"_10.txt
	
	cp FR2_N_15.txt ../../../../elaboration_Pt/FR2_Fin_var_N_"$j"_15.txt
	cp FR2_D_15.txt ../../../../elaboration_Pt/FR2_Fin_var_D_"$j"_15.txt
	
	cp FR2_N_20.txt ../../../../elaboration_Pt/FR2_Fin_var_N_"$j"_20.txt
	cp FR2_D_20.txt ../../../../elaboration_Pt/FR2_Fin_var_D_"$j"_20.txt

	cp FR2_N_25.txt ../../../../elaboration_Pt/FR2_Fin_var_N_"$j"_25.txt
	cp FR2_D_25.txt ../../../../elaboration_Pt/FR2_Fin_var_D_"$j"_25.txt
	cd ..
	d=$c
	c=`expr $d + 1`
done 


cd /data/atlastop3/cervato/qt/elaboration_Pt

########################################           Fin VARIABLE          ##########################################
for b in "FR2"
do
	for dm in 10 15 20 25
	do	
		for c in "_N" "_D"
		do
			mv "$b"_Fin_var"$c"_-9_"$dm".txt temp.txt 
			for j in -8 -7 -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6 7 8 9 10 
			do
				cat temp.txt "$b"_Fin_var"$c"_"$j"_"$dm".txt > file3.txt
				mv file3.txt temp.txt
			done
			mv temp.txt "$b"_FIN_VAR"$c"_"$i"_"$z"_"$dm".txt 
		done
	done 
done



echo "The end..."
