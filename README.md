# Qualification_Task_BC

# Soft *b*-hadron tagging
In this Readme file, you can find the work description and a step-by-step guide concerning how to do the analysis that I performed.\
In the following paragraphs, I describe how I usually run the code, producing the results concerning the efficiency estimation and the optimization of the JobOption file settings. The basic idea is to have a specific script for each plot or result that you want to compute. \
Summing up, in order to perform a specific analysis, you should run a specific code (or better, a set of scripts) that I will describe in the following.

# SoftBFinderTool
The scope of this work is to optimize a soft-*b*-hadron tagging tool, called *SoftBFinderTool*. The distinction between well and badly reconstructed SVs is performed by applying some cuts on the BDT weights as well as on kinematic variables. In particular, there are three cuts that have the larger impact on the efficiency and Fake Rate (FR) of the tool:
* **IniBDTCut**: a cut on the BDT weight assigned at each two-track-vertices before the merging procedure (i.e. the procedure that merge together several two-track-vertices forming multi-track-vertices).
* **CosSVPV**: cut on the cosine of the angle between the vector that connects the secondary vertex to
the primary one, and the full vertex momentum. For instance, if we impose *CosSVPV* = 0.5 then, we exclude everything with *CosSVPV* lower than 0.5.
* **FinBDTCut**: same meaning of *IniBDTCut* but it is applied at the end of the merging procedure and just on the subset of two-track-vertices that the algorithm does not manage to put toghter in one (or more) multi-track-SV(s).

# Samples
The samples used in this project are listed in the following tables. To perfom the performance comparison between samples with and without soft *b*-hadron, we use:

| Sample name   | Sample desription    | Sample DID                                                                                                                   |
|-------------------|------------|------------------------------------------------------------------------------------------------------------------|
|ttbar | ttbar reference sample  | mc16_13TeV:mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.merge.AOD.e6337_e5984_s3126_r12305_r12253_r12305_r12298 |
|BB_direct| Sample with soft BH (max BH energy = 10 GeV)   | mc16_13TeV:mc16_13TeV.436980.MGPy8EG_A14N23LO_BB_direct_600_590_MET200.merge.AOD.e7417_e5984_a875_r10724_r10726               |
|Z0_js| Sample BH-less | mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.AOD.e3569_s3126_d1480_d1471_r10681_r10682_der166317305        |

We check the tool performance using also other samples with soft BH that are listed in the ollowing table.

| Sample name | Sample description    | Sample DID                                                                                 |
|-------------|------|------------------------------------------------------------------------------------------------------------------------------|
| 5GeV        | Max BH energy = 5 GeV |mc16_13TeV:mc16_13TeV.436981.MGPy8EG_A14N23LO_BB_direct_600_595_MET200.merge.AOD.e7417_e5984_a875_r10724_r10726 |
| 15GeV       | Max BH energy = 15 GeV| mc16_13TeV:mc16_13TeV.436979.MGPy8EG_A14N23LO_BB_direct_600_585_MET200.merge.AOD.e7417_e5984_a875_r10724_r10726               |
| 25GeV       | Max BH energy = 25 GeV| mc16_13TeV:mc16_13TeV.436978.MGPy8EG_A14N23LO_BB_direct_600_575_MET200.merge.AOD.e7417_e5984_a875_r10724_r10726        |


# Analysis strategy
In a few words, the work-flow of this project consists in:
1. Modify the JobOption file, by changing the value of the three cuts described above;
2. Run Athena (release 22.0.41.8);
3. Analyze the results:
    1. First we have to define three working-points using the ttbar reference sample. To do that, we proceed with two different steps:
        1. The first step is to perform the a two-dimensional anlysis. The aim of this analysis is to vary at the same time the two cut that have the larger impact on the efficiency and FR values (i.e. *FinBDTCut* and *IniBdtCut*) and, for each possible combination of these two cuts, estimate the algorithm performance. The names of scripts and folders related with this analysis, end with the label *ScatterIniFin*.
        2. Then, we do a fine-tuning by varying the *CosSVPV* value. In this case, the names end with the label *CosSVPV* or *Cos_var*.
    2. After the working-points definition, we check the performance of the tool using other MC samples.


# Scripts and folders organization
The aim of this project consists in changing the value of the three cuts of interest in the JobOption file, run Athena and, in the end, analyze the results estimating the efficiency and the fake rate. 
The whole analysis is mainly perfomed by running shell scripts. In this section we describe how to reproduce the analysis and the meaning of each script.
## Modify the JobOption file and run Athena
### Folder organization
The organization of the Athena folder is typical. We have three folders:
1. **athena**;
2. **build**;
3. **run**: this is the more important for the moment and we want to list also the subfolders here:
    * **INPUT**: in this folder the samples should be stored, divided in sub-folders (this is truth for all the samples but the ttbar sample, since we run Athena on the Grid with it). The algorithm will sun over all the files that are stored in the subfolders, depending on the chosen sample. In particular, we have the following subfolders:
        * 5GeV
        * 10GeV
        * 15GeV
        * 25GeV
        * Z0_js
    * **Setting_tuning**: in this folder we put the JobObtion files and the Athena phyton script, divided in subfolders corrispondent to the different samples. In this way, if you need to modify somethings in these files, you can do it here and, automatically, the changes will be implemented in all the files just by running the scripts described in the next section. 
    * **Scatter_Ini_Fin_analysis**: here we have just one folder, because the two-dimensional analysis is performed just for the ttbar reference sample:
        * ttbar: here there are 210 subfolders, one for each possible combination of the values of *IniBDTCUt* and *CosSVPV*. In each of these subfolders, are stored the output files of the algorithm that are named **TrackExampleSV_i.hist.root** and **resultsSV_i.root**, where *i* is a progressive number that corresponds to the subfolder name. 
    * **CosSVPV_analysis**: again just one subfolder (ttbar) with the same structure as the one just described. 
    * **WP validation**: in this folder the performance check of the WPs is done. We want to run Athena over the samples with soft BHs and over the sample BH-less, using the value defined by the WPs for the three cuts. In this folder, also the step 3 of the section *Analysis strategy* (Analize the results) is implemented. Thus, here, the subfolders contain not just the results from Athena, but also those about the WPs performance check. We will talk about the script and the WPs performance estimation later, for the moment we just list the subfolder that you can find here:
        * BB_5GeV
        * BB_direct (which corresponds to the 10GeV sample)
        * BB_15GeV
        * BB_25GeV
        * Z0_js


### Running Athena
The very first step is to setup Athena:

```
asetup Athena,22.0.41.8
```
And also to build the environment:

```
cd ./build
cmake ../athena/Projects/WorkDir/
make
source ./x86_64-centos7-gcc8-opt/setup.sh
cd ../run

```

After that, we can proceed with the analysis. The first two steps listed in the section *Analysis strategy* (Modify the JobOption file and run Athena) change depending on wheter one wants to run Athena on the Grid or not. For instance, to define the WPs we run over a huge amount of events (2 milion), thus it is convenient to run Athena on the Grid. But for the other samples this is not the case, so we have used the local cluster (SiMPLE sluster) that exploit the SLURM batch system.

#### Running on the Grid

There exists two scripts:
1. **Grid_script_ScatterIniFin.sh** that performs the two-dimensional analysis;
2. **Grid_script_CosSVPV.sh** that performs the fine-tuning.
To run them you should just type:

```
bash <script_name>
```
The results must be stored as described in the *Folder organization* section, and to do that you can run:
```
cd Scatter_Ini_Fin_analysis
```
and then:
```
bash rucio_download_script.sh
```

#### Running on a local cluster with SLURM
We check the WPs performance on other samples on the local cluster. Once again, all you need to do is to run an ad hoc shell script:

```
cd WP_validation
bash WP_Script.sh
```
