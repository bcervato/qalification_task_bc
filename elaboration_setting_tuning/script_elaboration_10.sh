#!/bin/sh
#Autor: Beatrice Cervato
#Changing some var. names to obtain the desidered analysis

#Please, copy the analysis that you whant to perform and paste it in the uncommented part of the script.
#REMEMBER to modify the value of j, i or z.....

echo "What analysis do you want to perform? Please, enter:"
echo "1 for cos variable, IniBDT = parameter"
echo "2 for cos variable, FinBDT = parameter"
echo "3 for FinBDT variable, cos = parameter"
echo "4 for FinBDT variable, IniBDT = parameter"
echo "5 for IniBDT variable, cos = parameter"
echo "6 for IniBDT variable, FinBDT = parameter"

#index = $(cat)
#read index


# index = 1 => VAR = cos, PAR = Ini
for b in "EvtEff" "FR1" "FR2" 
do
    #echo "What is the fixed value for the FinBDT cut ? Please, enter the number *10 (1 instead of 0.1)"
    #j = $(cat)
    j=0 ########################################### MODIFY #########################################
    z=4
    temp=11
    sed -i 's/float index=0/float index=1/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        i=`expr $f - $temp`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$i'_'$j'_COS_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/IniBDTCut = '$i'e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/IniBDTCut = '$i'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/cosSVPVCut/g' elaboration.C
    sed -i 's/SAVE_NAME/AAA_'$b'_Var-cos_Par-Ini_Fin-0_'$j'/g' elaboration.C
    sed -i 's/S_N_pt/AAA_PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C    
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=1/float index=0/g' elaboration.C


# index = 2 => cos variable, FinBDT = parameter
for b in "EvtEff" "FR1" "FR2" 
do
    #echo "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #read i
    #i = $(cat)
    i=-7 ################################## MODIFY #######################################
    z=0
    sed -i 's/float index=0/float index=2/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        j=`expr $f - 3`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$i'_'$j'_COS_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/cosSVPVCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-cos_Par-Fin_Ini-0_'$i'/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=2/float index=0/g' elaboration.C

<<COMMENT
for b in "EvtEff" "FR1" "FR2" 
do
    #echo "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #read i
    #i = $(cat)
    i=-7 ##################################3 MODIFY #######################################
    sed -i 's/float index=0/float index=2/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        j=`expr $f - 3`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$i'_'$j'_COS_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/cosSVPVCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-cos_Par-Fin_Ini-0_'$i'/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=2/float index=0/g' elaboration.C
COMMENT


#if ($index="3") #VAR = Fin, PAR = cos
for b in "EvtEff" "FR1" "FR2"
do
   #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #read i
    i=-7 ####################################### MODIFY #######################################
    j=0
    temp=10
    sed -i 's/float index=0/float index=3/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        z=`expr $f + $f - $temp`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$i'_'$z'_Fin_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/cosSVPVCut = '$z'e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/cosSVPVCut = '$z'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/FinBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Fin_Par-cos_Ini-0_'$i'/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=3/float index=0/g' elaboration.C






#if ($index="4") #VAR = Fin, PAR = Ini
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    z=4 ######################################## MODIFY #####################################
    j=0
    temp=11
    sed -i 's/float index=0/float index=4/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        i=`expr $f - $temp`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$i'_'$z'_Fin_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/IniBDTCut = '$i'e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/IniBDTCut = '$i'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/FinBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Fin_Par-Ini_Cos_'$z'/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=4/float index=0/g' elaboration.C





#if ($index="5") #VAR = IniBDT, PAR = Cos
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the FinBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    j=0 ######################################## MODIFY #####################################3333
    i=-7
    temp=1
    sed -i 's/float index=0/float index=5/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        z=`expr $f - $temp`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$j'_'$z'_Ini_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/cosSVPVCut = '$z'e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/cosSVPVCut = '$z'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/IniBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Ini_Par-cos_Fin_'$j'/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=5/float index=0/g' elaboration.C







#if ($index="6") #VAR = IniBDT, PAR = FinBDT
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the FinBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    z=4 #(Default = 0.4) ######################################## MODIFY #####################################
    i=-7
    sed -i 's/float index=0/float index=6/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        j=`expr $f - 3`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$j'_'$z'_Ini_VAR.txt/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/IniBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Ini_Par-Fin_Cos_'$z'/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=6/float index=0/g' elaboration.C



echo "The end"
