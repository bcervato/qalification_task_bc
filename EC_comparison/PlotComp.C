#include <fstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TPDF.h"
#include "TMath.h"
#include "TLegend.h"

#include "TGedPatternSelect.h"
#include "TGColorSelect.h"
#include "TGColorDialog.h"
#include "TColor.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TSelectorDraw.h"
#include "TGTab.h"
#include "TGFrame.h"
#include "TGMsgBox.h"
#include "TClass.h"
#include "TLegend.h"
#include "TColor.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TEfficiency.h"
#include "THStack.h"

#include "THStack.h"
#include "TText.h"


using namespace std;
void PlotComp ()
{
	
	    #ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
	#endif

   SetAtlasStyle();
    gStyle->SetErrorX(0);
    //TGraphErrors *g_VAR1 = new TGraphErrors();
   
	
    TEfficiency *h1;
    TEfficiency *h2;
    TEfficiency *h3;
    TEfficiency *h4;
    TEfficiency *h5;
    TEfficiency *h6;
    TEfficiency *h7;
    TEfficiency *h8;
    TEfficiency *h9;
    TEfficiency *h10;
    TEfficiency *h11;
    TEfficiency *h12;
    
    TTree *hBH5;
    TTree *hBH10;
    TTree *hBH15;
    TTree *hBH25;    
    
    TEfficiency *hE5;
    TEfficiency *hE10;
    TEfficiency *hE15;
    TEfficiency *hE25;
    
    TGraph *g1_5;
    TGraph *g1_10;
    TGraph *g1_15;
    TGraph *g1_25;
    TGraph *g2_5;
    TGraph *g2_10;
    TGraph *g2_15;
    TGraph *g2_25;
    

	float e=0;
    int i=0;
    float index=0;	
	
	TFile* file_01 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_25/Fin_var/EC_EvtEff.root");
	TFile* file_02 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_25/Fin_var/EC_FR1.root");
	TFile* file_03 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_25/Fin_var/EC_FR2.root");
	
	
	TFile* file_04 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_15/Fin_var/EC_EvtEff.root");
	TFile* file_05 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_15/Fin_var/EC_FR1.root");
	TFile* file_06 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_15/Fin_var/EC_FR2.root");
	
	TFile* file_07 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST/Fin_var/EC_EvtEff.root");
	TFile* file_08 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST/Fin_var/EC_FR1.root");
	TFile* file_09 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST/Fin_var/EC_FR2.root");

	TFile* file_10 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_5/Fin_var/EC_EvtEff.root");
	TFile* file_11 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_5/Fin_var/EC_FR1.root");
	TFile* file_12 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/elaboration_ST_5/Fin_var/EC_FR2.root");
	
	TFile* fnBH5 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/5_JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/trackExampleSV.hist.root");
	TFile* fnBH10 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/trackExampleSV.hist.root");
	TFile* fnBH15 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/15_JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/trackExampleSV.hist.root");
	TFile* fnBH25 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/25_JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/trackExampleSV.hist.root");

	TFile* fE5 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/5_JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/Plots.root");
	TFile* fE10 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/Plots.root");
	TFile* fE15 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/15_JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/Plots.root");
	TFile* fE25 = TFile::Open("/eos/user/b/bcervato/SWAN_projects/QT/qt_bc/25_JOTuning/Fin_var/Fin_var_Ini_-7_cos_4_HIGHSTAT/Fin_0/Plots.root");


	
	//TFile* file_10 = TFile::Open("Plots_10.root");
	file_01->GetObject("DENO_clone", h1);
	file_02->GetObject("DENO_clone", h2);
	file_03->GetObject("DENO_clone", h3);
	file_04->GetObject("DENO_clone", h4);	
	file_05->GetObject("DENO_clone", h5); 	
	file_06->GetObject("DENO_clone", h6);	
	file_07->GetObject("DENO_clone", h7);
	file_08->GetObject("DENO_clone", h8);
	file_09->GetObject("DENO_clone", h9);
	file_10->GetObject("DENO_clone", h10);
	file_11->GetObject("DENO_clone", h11);
	file_12->GetObject("DENO_clone", h12);
	h1->SetLineColor(kRed-4);
	h2->SetLineColor(kRed-4);
	h3->SetLineColor(kRed-4);
	h4->SetLineColor(kBlue-4);
	h5->SetLineColor(kBlue-4);
	h6->SetLineColor(kBlue-4);
	h7->SetLineColor(kGreen+3);
	h8->SetLineColor(kGreen+3);
	h9->SetLineColor(kGreen+3);
	h10->SetLineColor(kOrange-3);
	h11->SetLineColor(kOrange-3);
	h12->SetLineColor(kOrange-3);
	
	h1->SetMarkerColor(kOrange-3);
	h2->SetMarkerColor(kRed-4);
	h3->SetMarkerColor(kGreen-3);
	h4->SetMarkerColor(kAzure+10);
	h5->SetMarkerColor(kBlue-4);
	h6->SetMarkerColor(kCyan+2);
	h7->SetMarkerColor(kViolet+1);
	h8->SetMarkerColor(kRed+3);
	h9->SetMarkerColor(kGreen+3);
	/*h10->SetMarkerColor(kYellow-8);
    */
    h1->SetMarkerStyle(15);
    h2->SetMarkerStyle(15);
    h3->SetMarkerStyle(15);
    h4->SetMarkerStyle(15);
    h5->SetMarkerStyle(15);
    h6->SetMarkerStyle(15);
    h7->SetMarkerStyle(15);
    h8->SetMarkerStyle(15);
    h9->SetMarkerStyle(15);
    h10->SetMarkerStyle(15);
    h11->SetMarkerStyle(15);
    h12->SetMarkerStyle(15);
    
    h1->SetLineWidth(3);
	h2->SetLineWidth(3);
	h3->SetLineWidth(3);
	h4->SetLineWidth(3);
	h5->SetLineWidth(3);
	h6->SetLineWidth(3);
	h7->SetLineWidth(3);
	h8->SetLineWidth(3);
	h9->SetLineWidth(3);
	h10->SetLineWidth(3);
	h11->SetLineWidth(3);
	h12->SetLineWidth(3);
    
    h4->SetTitle("Event Efficiency;FinBDTCut;EvtEff");
    h2->SetTitle("Event Efficiency;FinBDTCut;FR1");
    h3->SetTitle("Event Efficiency;FinBDTCut;FR2");
   
   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
    
    fnBH5->GetObject("Event", hBH5);
	fnBH10->GetObject("Event", hBH10);
	fnBH15->GetObject("Event", hBH15);
	fnBH25->GetObject("Event", hBH25);	
	
	hBH25->SetLineColor(kRed-4);
	hBH15->SetLineColor(kBlue-4);
	hBH10->SetLineColor(kGreen+3);
	hBH5->SetLineColor(kOrange-3);
	
	hBH25->SetMarkerStyle(0);
    hBH15->SetMarkerStyle(0);
    hBH10->SetMarkerStyle(0);
    hBH5->SetMarkerStyle(0);
    
    //-----------------------------------------------------------
    
    fE5->GetObject("bPtAll_clone", hE5);
	fE10->GetObject("bPtAll_clone", hE10);
	fE15->GetObject("bPtAll_clone", hE15);
	fE25->GetObject("bPtAll_clone", hE25);
	hE25->SetLineColor(kRed-4);
	hE15->SetLineColor(kBlue-4);
	hE10->SetLineColor(kGreen+3);
	hE5->SetLineColor(kOrange-3);
	
	//*************************************************************
	
	file_01->GetObject("FR1", g1_25);
	file_04->GetObject("FR1", g1_15);
	file_07->GetObject("FR1", g1_10);
	file_10->GetObject("FR1", g1_5);
	file_01->GetObject("FR2", g2_25);
	file_04->GetObject("FR2", g2_15);
	file_07->GetObject("FR2", g2_10);
	file_10->GetObject("FR2", g2_5);
	g1_25->SetLineColor(kRed-4);
	g2_25->SetLineColor(kRed-4);
	g1_15->SetLineColor(kBlue-4);
	g2_15->SetLineColor(kBlue-4);
	g1_10->SetLineColor(kGreen+3);
	g1_10->SetLineColor(kGreen+3);
	g1_5->SetLineColor(kOrange-3);
	g1_5->SetLineColor(kOrange-3);	
	
	g1_25->SetMarkerColor(kRed-4);
	g2_25->SetMarkerColor(kRed-4);
	g1_15->SetMarkerColor(kBlue-4);
	g2_15->SetMarkerColor(kBlue-4);
	g1_10->SetMarkerColor(kGreen+3);
	g1_10->SetMarkerColor(kGreen+3);
	g1_5->SetMarkerColor(kOrange-3);
	g1_5->SetMarkerColor(kOrange-3);
	
	g2_25->SetLineColor(kRed-4);
	g2_25->SetLineColor(kRed-4);
	g2_15->SetLineColor(kBlue-4);
	g2_15->SetLineColor(kBlue-4);
	g2_10->SetLineColor(kGreen+3);
	g2_10->SetLineColor(kGreen+3);
	g2_5->SetLineColor(kOrange-3);
	g2_5->SetLineColor(kOrange-3);	
	
	g2_25->SetMarkerColor(kRed-4);
	g2_25->SetMarkerColor(kRed-4);
	g2_15->SetMarkerColor(kBlue-4);
	g2_15->SetMarkerColor(kBlue-4);
	g2_10->SetMarkerColor(kGreen+3);
	g2_10->SetMarkerColor(kGreen+3);
	g2_5->SetMarkerColor(kOrange-3);
	g2_5->SetMarkerColor(kOrange-3);	
	
	g1_25->GetXaxis()->SetTitle("Evnt Eff");
	g1_25->GetYaxis()->SetTitle("FR1");
	
	g2_25->GetXaxis()->SetTitle("Evnt Eff");
	g2_25->GetYaxis()->SetTitle("FR2");
	
	/*
	hBH25->SetFillColorAlpha(kWhite,0.9);
	hBH15->SetFillColorAlpha(5, 0.1);
	hBH10->SetFillColorAlpha(kBlue, 0.35);
	hBH5->SetFillColorAlpha(0,0.9);
	*/
	/*
	h1->SetMarkerColor(kOrange-3);
	h2->SetMarkerColor(kRed-4);
	h3->SetMarkerColor(kGreen-3);
	h4->SetMarkerColor(kAzure+10);
	
    
   
    
    hBH25->GetXaxis->SetTitle("number of BHs");
    h2->SetTitle("Event Efficiency;FinBDTCut;FR1");
    h3->SetTitle("Event Efficiency;FinBDTCut;FR2");
   */ 
   int a = 5;
   if (fabs(a)>2)
	std::cout<<fabs(a)<<" Con due par"<<std::endl;
    if (fabs(a>2))
		std::cout<<fabs(a>2)<<" Con una par"<<std::endl;
	//TGaxis::SetMaxDigits(3);
	TCanvas *cs1 = new TCanvas("cs1","cs1",10,10,1500,900);
    h1->Draw("AP");
    h4->Draw("SAME");
    h7->Draw("SAME");
    h10->Draw("SAME");
    gPad->Update(); 
	auto graph = h1->GetPaintedGraph(); 
	graph->SetMinimum(0);
	graph->SetMaximum(1); 
	gPad->Update(); 
    TLegend *leg1 = new TLegend(0.7, 1, .95, .8); //0.15,1,0.38,0.9
    leg1->SetBorderSize(0); 
	leg1->AddEntry(h1,"#Delta m = 25 GeV","F");
	leg1->AddEntry(h4,"#Delta m = 15 GeV","F");
	leg1->AddEntry(h7,"#Delta m = 10 GeV","F");
	leg1->AddEntry(h10,"#Delta m = 5 GeV","F");
    leg1->SetFillColor(kWhite);
	leg1->Draw();
    cs1->SaveAs("EvtEff_comp.png");
    
    TCanvas *cs2 = new TCanvas("cs2","cs2",10,10,1500,900);
    h11->Draw("AP");
    h5->Draw("SAME");
    h8->Draw("SAME");
    h2->Draw("SAME");
    gPad->Update(); 
	auto graph2 = h11->GetPaintedGraph(); 
	graph2->SetMinimum(0);
	graph2->SetMaximum(0.33); 
	gPad->Update(); 
    TLegend *leg2 = new TLegend(0.7, 1, .95, .8); //0.12,0.75,0.38,0.9
    leg2->SetBorderSize(0);
	leg2->AddEntry(h2,"#Delta m = 25 GeV","F");
	leg2->AddEntry(h5,"#Delta m = 15 GeV","F");
	leg2->AddEntry(h8,"#Delta m = 10 GeV","F");
	leg2->AddEntry(h11,"#Delta m = 5 GeV","F");
    leg2->SetFillColor(kWhite);
	leg2->Draw();
    cs2->SaveAs("FR1_comp.png");

    TCanvas *cs3 = new TCanvas("cs3","cs3",10,10,1500,900);
    h12->Draw("AP");
    h6->Draw("SAME");
    h9->Draw("SAME");
    h3->Draw("SAME");
    gPad->Update(); 
	auto graph3 = h12->GetPaintedGraph(); 
	graph3->SetMinimum(0);
	graph3->SetMaximum(0.33); 
	gPad->Update();
    TLegend *leg3 = new TLegend(0.7, 1, .95, .8); //0.12,0.75,0.38,0.9
    leg3->SetBorderSize(0);
	leg3->AddEntry(h3,"#Delta m = 25 GeV","F");
	leg3->AddEntry(h6,"#Delta m = 15 GeV","F");
	leg3->AddEntry(h9,"#Delta m = 10 GeV","F");
	leg3->AddEntry(h12,"#Delta m = 5 GeV","F");
    leg3->SetFillColor(kWhite);
	leg3->Draw();
    cs3->SaveAs("FR2_comp.png");
    
    /*
    TCanvas *cs4 = new TCanvas("cs4","cs4",10,10,1500,900);
    //hs->Draw();
    hBH25->Draw("nBHad", "" , "HIST E");
    //hBH25->Draw("nBHad", "" , "HIST E");
	hBH15->Draw("nBHad", "" , "SAME HIST E");
	hBH10->Draw("nBHad", "" , "SAME HIST E");
	hBH5->Draw("nBHad", "" , "SAME HIST E");
	TLegend *leg4 = new TLegend(0.7, 1, .95, .8); //0.12,0.75,0.38,0.9
    leg4->SetBorderSize(0);
	leg4->AddEntry(hBH25,"#Delta m = 25 GeV","F");
	leg4->AddEntry(hBH15,"#Delta m = 15 GeV","F");
	leg4->AddEntry(hBH10,"#Delta m = 10 GeV","F");
	leg4->AddEntry(hBH5,"#Delta m = 5 GeV","F");
    leg4->SetFillColor(kWhite);
	leg4->Draw();
    cs4->SaveAs("nBH_comp.png");
	
	
	
	TCanvas *cs5 = new TCanvas("cs5","cs5",10,10,1500,900);
	hBH25->Draw("BHadEta", "" , "HIST E");
	hBH15->Draw("BHadEta", "" , "SAME HIST E");
	hBH10->Draw("BHadEta", "" , "SAME HIST E");
	hBH5->Draw("BHadEta", "" , "SAME HIST E");
	TLegend *leg5 = new TLegend(0.7, 1, .95, .8); //0.12,0.75,0.38,0.9
    leg5->SetBorderSize(0);
	leg5->AddEntry(hBH25,"#Delta m = 25 GeV","F");
	leg5->AddEntry(hBH15,"#Delta m = 15 GeV","F");
	leg5->AddEntry(hBH10,"#Delta m = 10 GeV","F");
	leg5->AddEntry(hBH5,"#Delta m = 5 GeV","F");
    leg5->SetFillColor(kWhite);
	leg5->Draw();
    cs5->SaveAs("BHadEta_comp.png");
    
    hE5->SetTitle("BH reconstruction Efficiency;BH Pt [MeV];#epsilon");
    TCanvas *cs6 = new TCanvas("cs6","cs6",10,10,1500,900);
    hE5->Draw("AP");
    hE15->Draw("SAME");
    hE25->Draw("SAME");
    hE10->Draw("SAME");
    TLegend *leg6 = new TLegend(0.15,1,0.38,0.8); //0.15,1,0.38,0.9
    leg6->SetBorderSize(0); 
	leg6->AddEntry(hE25,"#Delta m = 25 GeV","F");
	leg6->AddEntry(hE15,"#Delta m = 15 GeV","F");
	leg6->AddEntry(hE10,"#Delta m = 10 GeV","F");
	leg6->AddEntry(hE5,"#Delta m = 5 GeV","F");
    leg6->SetFillColor(kWhite);
	leg6->Draw();
    cs6->SaveAs("PtEff_comp.png");
		
		*/
	
	TCanvas *cs7 = new TCanvas("cs7","cs7",10,10,1500,900);
    g1_25->Draw("APL");
    g1_15->Draw("PL SAME");
    g1_10->Draw("PL SAME");
    g1_5->Draw("PL SAME");
    TLegend *leg7 = new TLegend(0.7, 1, .95, .8); //0.15,1,0.38,0.9
    leg7->SetBorderSize(0); 
	leg7->AddEntry(g1_25,"#Delta m = 25 GeV","F");
	leg7->AddEntry(g1_15,"#Delta m = 15 GeV","F");
	leg7->AddEntry(g1_10,"#Delta m = 10 GeV","F");
	leg7->AddEntry(g1_5,"#Delta m = 5 GeV","F");
    leg7->SetFillColor(kWhite);
	leg7->Draw();
    cs7->SaveAs("Correlation_FR1_comp.png");


	TCanvas *cs8 = new TCanvas("cs8","cs8",10,10,1500,900);
    g2_25->Draw("APL");
    g2_15->Draw("PL SAME");
    g2_10->Draw("PL SAME");
    g2_5->Draw("PL SAME");
    TLegend *leg8 = new TLegend(0.7, 1, .95, .8); //0.15,1,0.38,0.9
    leg8->SetBorderSize(0); 
	leg8->AddEntry(g1_25,"#Delta m = 25 GeV","F");
	leg8->AddEntry(g1_15,"#Delta m = 15 GeV","F");
	leg8->AddEntry(g1_10,"#Delta m = 10 GeV","F");
	leg8->AddEntry(g1_5,"#Delta m = 5 GeV","F");
    leg8->SetFillColor(kWhite);
	leg8->Draw();
    cs8->SaveAs("Correlation_FR2_comp.png");
	
  return 0;
}


