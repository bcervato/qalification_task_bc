#define Eff_tuning_cxx
#include "Eff_tuning.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <fstream>
#include <TFile.h>
#include <TColor.h>
#include <iostream>
#include <TImage.h>

void Eff_tuning::Loop()
{
if (fChain == 0) return;
	Long64_t nentries = fChain->GetEntriesFast();
	TH1::SetDefaultSumw2();
	//gStyle->SetLineStyle(1);
	
   TH1F * h_bPtAll =new TH1F("bPtAll","All Bhadron Pt",50,0.,2.e4);
   TH1F * h_bPtMat =new TH1F("bPtMat","Matched Bhadron Pt",50,0.,2.e4);
   TH1F * h_bPtEff =new TH1F("bPtEff","Bhadron Efficiency vs Pt",50,0.,2.e4);
    
   h_bPtEff->GetXaxis()->SetTitle("BH Pt [MeV]");
   h_bPtEff->GetYaxis()->SetTitle("Efficiency");
   
   Long64_t nbytes = 0, nb = 0;   

   float OutPtCut_counter=0, nVrtTOT=0, VrtTrk0=0, VrtTrk0TOT=0, nonMatchedSVTOT=0;
   int nEventsWithMatchedSV=0; //Number of events with at least one SV matched to b-hadron
   int NonAcc=0, NonAccTot=0, count_non_reco=0, event_non_reco=0;
   ofstream fout("EvtEff.txt");
   ofstream fout1("FR1.txt");
   ofstream fout2("FR2.txt"); 
    
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if(nVrt==0)continue;   //Skip events without reconstructed SVs
      
      if(OutPtCut<=2)
      	OutPtCut_counter++;
      
      nVrtTOT+=nVrt;
       
      int nMatchedSV=0;   // Number of matched SVs in the given event
      NonAcc=0;
      VrtTrk0=0;
      for(int iv=0; iv<nVrt; iv++){
        if(VrtTrkHF[iv]==0){
         // VrtTrk0++;
          VrtTrk0TOT++;
        }
		if(fabs(BHadEta[matchVrtBH[iv]]>2.5)){
			NonAcc++;
			NonAccTot++;
		}
		else{
	      	if(dRVrtBH[iv]>0.2) {
                nonMatchedSVTOT++;
                continue;      // Effectively no matching	
            }
			nMatchedSV++;                      // count number of SVs matched to a b-hadron
			h_bPtMat->Fill(BHadPt[matchVrtBH[iv]],1.);  // Pt of the matched b-hadron for the given SV
		}
      }
      if(nMatchedSV)
          nEventsWithMatchedSV++;   //Number of events with at least one SV matched to b-hadron


       
      for(int ib=0; ib<nBHad; ib++){
           if(fabs(BHadEta[ib]>2.5)){
			   //out_eta++;
			   continue;
			   }
			h_bPtAll->Fill(BHadPt[ib],1.);  // Fill pt distribution of ALL b-hadrons
      }
       
       
    }  //--End of event loop
    
    

    h_bPtEff->Divide(h_bPtMat, h_bPtAll, 1.,1.,"O"); // Calculate ratio of histograms - pt-dependent B-hadron efficiency
    std::cout<<" Overall B-hadron detection efficiency per event ="<<(float)nEventsWithMatchedSV/((float)(nentries)-OutPtCut_counter)<<'\n';
    std::cout<<" Fake Rate (1) = (#SV with dR > 0.2)/(# SV tot) = "<<(float)nonMatchedSVTOT/nVrtTOT<<'\n';
    std::cout<<" Fake Rate (2) = (#SV with 0 BH-track inside)/(# SV tot) = "<<(float)VrtTrk0TOT/nVrtTOT<<'\n';
    fout<<(float)nEventsWithMatchedSV/((float)(nentries)-OutPtCut_counter)<<'\n';
    fout1<<(float)(nonMatchedSVTOT)/(float)(nVrtTOT)<<'\n';
    fout2<<(float)(VrtTrk0TOT)/(float)(nVrtTOT)<<'\n';
    
    

   TImage *img = TImage::Create();
    
   TCanvas *c = new TCanvas;
   h_bPtEff->Draw("HIST");
   img->FromPad(c);
   img->WriteImage("bPtEff.jpg");
   delete(c);

   
   TFile saving ("Plots.root","recreate");
  
   h_bPtEff->Draw();
   h_bPtEff->SetOption("HIST");
   h_bPtEff->Write(); 

 
   saving.Close();
}
