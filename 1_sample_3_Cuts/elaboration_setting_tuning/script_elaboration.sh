#!/bin/sh
#Autor: Beatrice Cervato
#Changing some var. names to obtain the desidered analysis

#Please, copy the analysis that you whant to perform and paste it in the uncommented part of the script.
#REMEMBER to modify the value of j, i or z.....

echo "What analysis do you want to perform? Please, enter:"
echo "1 for cos variable, IniBDT = parameter"
echo "2 for cos variable, FinBDT = parameter"
echo "3 for FinBDT variable, cos = parameter"
echo "4 for FinBDT variable, IniBDT = parameter"
echo "5 for IniBDT variable, cos = parameter"
echo "6 for IniBDT variable, FinBDT = parameter"

#index = $(cat)
#read index

# index = 2 => cos variable, FinBDT = parameter
for b in "EvtEff" "FR1" "FR2" 
do
    #echo "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #read i
    #i = $(cat)
    i=-8 ################################## MODIFY #######################################
    z=2
    sed -i 's/float index=0/float index=2/g' elaboration.C
    for f in 1 2 3 4 5 
    do
        j=`expr $f + $f - 4`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$i'_'$j'_COS_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/cosSVPVCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-cos_Par-Fin_Ini-0_'$i'/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=2/float index=0/g' elaboration.C

<<COMMENT

# PRELIMINARY RESULTS: just cos variable

for b in "EvtEff" "FR1" "FR2" 
do
    #echo "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #read i
    #i = $(cat)
    i=-10 ################################## MODIFY #######################################
    j=-2
    temp=14
    sed -i 's/float index=0/float index=1/g' elaboration.C
    for f in 1 2 3 4 5
    do
        z=`expr $f + $f + $f + $f - $temp`
        sed -i 's/INPUT_FILE_1/'$b'_BB_direct_-10_-2_COS_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_1/FinBDTCut = -2e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/cosSVPVCut = '$z'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/cosSVPVCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-cos/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=1/float index=0/g' elaboration.C




























#if ($index="3") #VAR = Fin, PAR = cos
for b in "EvtEff" "FR1" "FR2"
do
   #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #read i
    i=-10 ####################################### MODIFY #######################################
    j=0
    temp=14
    sed -i 's/float index=0/float index=3/g' elaboration.C
    for f in 1 2 3 4 5 
    do
        z=`expr $f + $f + $f + $f - $temp`
        sed -i 's/INPUT_FILE_'$f'/'$b'_BB_direct_'$i'_'$z'_Fin_VAR.txt/g' elaboration.C
        sed -i 's/MESSAGE_LEG_'$f'/cosSVPVCut = '$z'e-1/g' elaboration.C
        sed -i 's/Plots_'$f'/Plots_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/cosSVPVCut = '$z'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/FinBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Fin_Par-cos_Ini-0_'$i'/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_BB_direct_'$i'_'$j'_'$z'/g' elaboration.C

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=3/float index=0/g' elaboration.C

COMMENT


echo "The end"
