#include <fstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TPDF.h"
#include "TMath.h"
#include "TLegend.h"

#include "TGedPatternSelect.h"
#include "TGColorSelect.h"
#include "TGColorDialog.h"
#include "TColor.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TSelectorDraw.h"
#include "TGTab.h"
#include "TGFrame.h"
#include "TGMsgBox.h"
#include "TClass.h"
#include "TLegend.h"
#include "TColor.h"
#include "TStyle.h"
#include "TGaxis.h"

#include "THStack.h"
#include "TText.h"


using namespace std;
void elaboration ()
{

	    #ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
	#endif

   SetAtlasStyle();
     
    //TGraphErrors *g_VAR1 = new TGraphErrors();
    TGraph *g_VAR1 = new TGraph();
    TGraph *g_VAR2 = new TGraph();
    TGraph *g_VAR3 = new TGraph();
    TGraph *g_VAR4 = new TGraph();
    TGraph *g_VAR5 = new TGraph();



	
    TH1F *h1;
    TH1F *h2;
    TH1F *h3;
    TH1F *h4;
    TH1F *h5;


	float e=0;
    int i=0;
    float index=0;
    
    ifstream fin1("INPUT_FILE_1");
    ifstream fin2("INPUT_FILE_2");
	ifstream fin3("INPUT_FILE_3");
	ifstream fin4("INPUT_FILE_4");
	ifstream fin5("INPUT_FILE_5");

   
	TFile* file_01 = TFile::Open("Plots_1.root");
	TFile* file_02 = TFile::Open("Plots_2.root");
	TFile* file_03 = TFile::Open("Plots_3.root");
	TFile* file_04 = TFile::Open("Plots_4.root");
	TFile* file_05 = TFile::Open("Plots_5.root");

	file_01->GetObject("bPtEff", h1);
	file_02->GetObject("bPtEff", h2);
	file_03->GetObject("bPtEff", h3);
	file_04->GetObject("bPtEff", h4);	
	file_05->GetObject("bPtEff", h5); 	

	h1->SetLineColor(kOrange-3);
	h2->SetLineColor(kRed-4);
	h3->SetLineColor(kGreen-3);
	h4->SetLineColor(kAzure+10);
	h5->SetLineColor(kBlue-4);

    
    if (index == 1 || index == 2) //cos variable, IniBDT = parameter || FinBDT = parameter
    {
        i=1;
        do
        {
            fin1>>e;
            g_VAR1->SetPoint(i-1,(i*4-14)*0.1,e);
            //g_VAR1->SetPointError(i, 0,sqrt(e));
            i++;
        }while (!fin1.eof());  
        
        i=1;
        do
        {
            fin1>>e;
            g_VAR1->SetPoint(i-1,(i*4-14)*0.1,e);
            //g_VAR1->SetPointError(i, 0,sqrt(e));
            i++;
        }while (!fin1.eof());  
        
        i=1;
        do
        {
            fin2>>e;
            g_VAR2->SetPoint(i-1,(i*4-14)*0.1,e);
            i++;
        }while (!fin2.eof());  
        
        i=1;
        do
        {
            fin3>>e;
            g_VAR3->SetPoint(i-1,(i*4-14)*0.1,e);
            i++;
        }while (!fin3.eof());  
        
        i=1;
        do
        {
            fin4>>e;
            g_VAR4->SetPoint(i-1,(i*4-14)*0.1,e);
            i++;
        }while (!fin4.eof()); 
        
        i=1;
        do
        {
            fin5>>e;
            g_VAR5->SetPoint(i-1,(i*4-14)*0.1,e);
            i++;
        }while (!fin5.eof());  
        
    }
		
		
    g_VAR1->SetMarkerColor(kOrange-3);
    g_VAR2->SetMarkerColor(kRed-4);
    g_VAR3->SetMarkerColor(kGreen-3);
    g_VAR4->SetMarkerColor(kAzure+10);
    g_VAR5->SetMarkerColor(kBlue-4);
    
    TMultiGraph *mg = new TMultiGraph();

    mg->Add(g_VAR1,"cp");
    mg->Add(g_VAR2,"cp");
    mg->Add(g_VAR3,"cp");
    mg->Add(g_VAR4,"cp");
    mg->Add(g_VAR5,"cp");
    
    mg->GetXaxis()->SetTitle("X_AXIS_LABEL");
    mg->GetYaxis()->SetTitle("Y_AXIS_LABEL");

    TCanvas *cs = new TCanvas("cs","cs",10,10,1100,900);
    mg->Draw("a");
    TLegend *leg = new TLegend(); //0.12,0.75,0.38,0.9
    leg->SetBorderSize(0);
	leg->AddEntry(g_VAR1,"MESSAGE_LEG_1","P");
	leg->AddEntry(g_VAR2,"MESSAGE_LEG_2","P");
	leg->AddEntry(g_VAR3,"MESSAGE_LEG_3","P");
    leg->AddEntry(g_VAR4,"MESSAGE_LEG_4","P");
    leg->AddEntry(g_VAR5,"MESSAGE_LEG_5","P");
	leg->SetFillColor(kWhite);
	leg->Draw();
    cs->SaveAs("SAVE_NAME.png"); 
    
    
    
	TGaxis::SetMaxDigits(3);
	TCanvas *cs1 = new TCanvas("cs1","cs1",10,10,1800,1000);
    h1->SetMarkerStyle(0);
    h1->Draw("HIST E");
	h2->SetMarkerStyle(0);
    h2->Draw("HIST E SAME");
	h3->SetMarkerStyle(0);
    h3->Draw("HIST E SAME");
	h4->SetMarkerStyle(0);
    h4->Draw("HIST E SAME");
	h5->SetMarkerStyle(0);
	h5->Draw("HIST E SAME");


		
    
    TLegend *leg1 = new TLegend(0.12,0.75,0.38,0.9); //0.12,0.75,0.38,0.9
    leg1->SetBorderSize(0);
	leg1->AddEntry(h1,"MESSAGE_LEG_pt_1","F");
	leg1->AddEntry(h2,"MESSAGE_LEG_pt_2","F");
	leg1->AddEntry(h3,"MESSAGE_LEG_pt_3","F");
    leg1->AddEntry(h4,"MESSAGE_LEG_pt_4","F");
    leg1->AddEntry(h5,"MESSAGE_LEG_pt_5","F");

	leg1->SetFillColor(kWhite);
	leg1->Draw();
    cs1->SaveAs("S_N_pt.png");

  return 0;
}


