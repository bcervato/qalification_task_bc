#!/bin/sh
#Autor: Beatrice Cervato
#Efficiency analysis variing 3 parameters in the JobOption file.



Run_Root() {
	cp script_MakeClass.sh Constructor_MakeClass.sh Setting_tuning/"$1"/IniBDT_"$2"/FinBDT_"$3"/cosSVPV_"$4"/
	cd Setting_tuning/
	cp Eff_tuning.C "$1"/IniBDT_"$2"/FinBDT_"$3"/cosSVPV_"$4"/Eff_tuning1.C
	cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/Setting_tuning/"$1"/IniBDT_"$2"/FinBDT_"$3"/cosSVPV_"$4"/
	echo "$1 sample: v2tIniBDTCut = $2, v2tFinBDTCut = $3, cosSVPVCut = $4"
	bash Constructor_MakeClass.sh 
	mv Eff_tuning1.C Eff_tuning.C
	bash script_MakeClass.sh
	echo "========================================================================================"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cp Plots.root ../../../../../elaboration_setting_tuning/Plots_"$1"_"$2"_"$3"_"$4".root
	cp EvtEff.txt ../../../../../elaboration_setting_tuning/EvtEff_"$1"_"$2"_"$3"_"$4".txt
	cp FR1.txt ../../../../../elaboration_setting_tuning/FR1_"$1"_"$2"_"$3"_"$4".txt
	cp FR2.txt ../../../../../elaboration_setting_tuning/FR2_"$1"_"$2"_"$3"_"$4".txt
	cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/Setting_tuning/"$1"/IniBDT_"$2"/FinBDT_"$3"/cosSVPV_"$4"/
	cp NEvnt.txt ../../../../../elaboration_setting_tuning/NEvnt_"$1"_"$2"_"$3"_"$4".txt
	cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/
}



for a in "BB_direct" ##################"ttbar"
do
	for i in -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 #-10 -9 -8 -7 -6 -5
    do
        for j in -2 -1 0 1 2 3 4 5 6 7  #-2 -1 0 1 2
        do
            for z in -10 -8 -6 -4 -2 0 2 4 6 8 10 # 0 1 2 3 4 5 6
            do
				Run_Root $a $i $j $z 
            done
        done
	done
done
wait
<<COMMENT
a="BB_direct"
i=-5
for j in 0 1 2
do
    for z in 5 6
    do
        cp script_MakeClass_5_6.sh Setting_tuning/$a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
        cd Setting_tuning/
        cp Eff.C Eff.h $a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
        cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/Setting_tuning/$a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
        echo "$a sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
        bash script_MakeClass_5_6.sh
        echo "========================================================================================"
        echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        cp Plots.root ../../../../../elaboration_setting_tuning/Plots_"$a"_"$i"_"$j"_"$z".root
        cp EvtEff.txt ../../../../../elaboration_setting_tuning/EvtEff_"$a"_"$i"_"$j"_"$z".txt
        cp FR1.txt ../../../../../elaboration_setting_tuning/FR1_"$a"_"$i"_"$j"_"$z".txt
        cp FR2.txt ../../../../../elaboration_setting_tuning/FR2_"$a"_"$i"_"$j"_"$z".txt
        cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/
    done
done



a="BB_direct"
i=-5
for j in -1 -2
do
    for z in 0 1 2 3 4 5 6
    do
        cp script_MakeClass_5_6.sh Setting_tuning/$a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
        cd Setting_tuning/
        cp Eff.C Eff.h $a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
        cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/Setting_tuning/$a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
        echo "$a sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
        bash script_MakeClass_5_6.sh
        echo "========================================================================================"
        echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        cp Plots.root ../../../../../elaboration_setting_tuning/Plots_"$a"_"$i"_"$j"_"$z".root
        cp EvtEff.txt ../../../../../elaboration_setting_tuning/EvtEff_"$a"_"$i"_"$j"_"$z".txt
        cp FR1.txt ../../../../../elaboration_setting_tuning/FR1_"$a"_"$i"_"$j"_"$z".txt
        cp FR2.txt ../../../../../elaboration_setting_tuning/FR2_"$a"_"$i"_"$j"_"$z".txt
        cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/
    done
done



a="BB_direct"
i=-7
j=0
for z in 0 1 2 3
do
    cp script_MakeClass_5_6.sh Setting_tuning/$a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
    cd Setting_tuning/
    cp Eff.C Eff.h $a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
    cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/Setting_tuning/$a/IniBDT_$i/FinBDT_$j/cosSVPV_$z/
    echo "$a sample: v2tIniBDTCut = $i, v2tFinBDTCut = $j, cosSVPVCut = $z"
    bash script_MakeClass_5_6.sh
    echo "========================================================================================"
    echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    cp Plots.root ../../../../../elaboration_setting_tuning/Plots_"$a"_"$i"_"$j"_"$z".root
    cp EvtEff.txt ../../../../../elaboration_setting_tuning/EvtEff_"$a"_"$i"_"$j"_"$z".txt
    cp FR1.txt ../../../../../elaboration_setting_tuning/FR1_"$a"_"$i"_"$j"_"$z".txt
    cp FR2.txt ../../../../../elaboration_setting_tuning/FR2_"$a"_"$i"_"$j"_"$z".txt
    cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/
done


COMMENT


cd /eos/home-b/bcervato/SWAN_projects/QT/qt_bc/elaboration_setting_tuning/


#select the data that you want to consider in the analysis
#The code "elaboration.C" take into consideration just the EvtEff.txt, FR1.txt and FR2.txt. Thus make sure to build these three files with the "subfile" in which you are interessed.

########################################           cos VARIABLE          ##########################################
for b in  "EvtEff" "FR1" "FR2"
do
    for i in -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 #-10 -9 -8 -7 -6 -5
    do
        for j in -2 -1 0 1 2 3 4 5 6 7  #-2 -1 0 1 2
        do
            cp "$b"_BB_direct_"$i"_"$j"_-10.txt temp_"$j".txt
            for z in -8 -6 -4 -2 0 2 4 6 8 10 # 0 1 2 3 4 5 6 
            do
                cat temp_"$j".txt "$b"_BB_direct_"$i"_"$j"_"$z".txt > file3.txt
                mv file3.txt temp_"$j".txt
            done
            mv temp_"$j".txt "$b"_BB_direct_"$i"_"$j"_COS_VAR.txt 
        done
    done
done

########################################           FinBDT VARIABLE          ##########################################

for b in "EvtEff" "FR1" "FR2"
do
    for i in -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 #-10 -9 -8 -7 -6 -5
    do
        for z in -10 -8 -6 -4 -2 0 2 4 6 8 10 # 0 1 2 3 4 5 6
        do
            cp "$b"_BB_direct_"$i"_-2_"$z".txt temp_"$z".txt
            for j in -1 0 1 2 3 4 5 6 7  # -1 0 1 2 
            do
                cat temp_"$z".txt "$b"_BB_direct_"$i"_"$j"_"$z".txt > file3.txt
                mv file3.txt temp_"$z".txt
            done
            mv temp_"$z".txt "$b"_BB_direct_"$i"_"$z"_Fin_VAR.txt 
        done
    done
done

########################################           IniBDT VARIABLE          ##########################################

for b in "EvtEff" "FR1" "FR2"
do
    for j in -2 -1 0 1 2 3 4 5 6 7  #-2 -1 0 1 2
    do
        for z in -10 -8 -6 -4 -2 0 2 4 6 8 10 # 0 1 2 3 4 5 6
        do
            cp "$b"_BB_direct_-10_"$j"_"$z".txt temp_"$z".txt
            for i in -9 -8 -7 -6 -5 -4 -3 -2 -1 #-10 -9 -8 -7 -6 -5
            do
                cat temp_"$z".txt "$b"_BB_direct_"$i"_"$j"_"$z".txt > file3.txt
                mv file3.txt temp_"$z".txt
            done
            mv temp_"$z".txt "$b"_BB_direct_"$j"_"$z"_Ini_VAR.txt 
        done
    done
done

#bash script_elaboration.sh

