#!/bin/sh
#Autor: Beatrice Cervato
#Changing some var. names to obtain the desidered analysis

#Please, copy the analysis that you whant to perform and paste it in the uncommented part of the script.
#REMEMBER to modify the value of j, i or z.....

echo "What analysis do you want to perform? Please, enter:"
echo "1 for cos variable, IniBDT = parameter"
echo "2 for cos variable, FinBDT = parameter"
echo "3 for FinBDT variable, cos = parameter"
echo "4 for FinBDT variable, IniBDT = parameter"
echo "5 for IniBDT variable, cos = parameter"
echo "6 for IniBDT variable, FinBDT = parameter"

#index = $(cat)
#read index
echo "Fine prima parte"






#if ($index="4") #VAR = Fin ##########################new######################
cp Backup_elaboration.C elaboration.C
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    z=4 ######################################## MODIFY #####################################
    temp=1
    t=12
    i=-7
    sed -i 's/float index=0/float index=4/g' elaboration.C
    sed -i 's/INPUT_FILE_1/'$b'_FIN_VAR_'$i'_'$z'_HS.txt/g' elaboration.C
    sed -i 's/INPUT_FILE_2/'$b'_FIN_VAR_N_'$i'_'$z'_HS.txt/g' elaboration.C
    sed -i 's/INPUT_FILE_3/'$b'_FIN_VAR_D_'$i'_'$z'_HS.txt/g' elaboration.C
    sed -i 's/MESSAGE_LEG_1/IniBDTCut = '$i'e-1 CosSVPV = '$z'e-1/g' elaboration.C
    for f in 1 2 3 4 5
    do
        c=`expr $f - $temp`
        j=`expr $f + $f + $f + $f - $t`
        sed -i 's/Plots_'$f'.root/Plots_Fin_var_'$j'_HS.root/g' elaboration.C #Plots_Fin_var_'$i'_'$j'_'$z'.root/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/FinBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Fin/g' elaboration.C
    sed -i 's/SAVE_NAME_EFF/'$b'_Var-Fin_EFF/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_FINVAR/g' elaboration.C
    sed -i 's/myfile.root/EC_'$b'.root/g' elaboration.C

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=4/float index=0/g' elaboration.C





cp Backup_correlation.C correlation.C
for g in "FR1" "FR2"
do
	echo "Ciao"
    ##########################    FRs VS Eff    ####################
	z=4 ######################################## MODIFY #####################################
    t=3
    i=-7
    sed -i 's/INPUT_FILE_Y/'$g'_FIN_VAR_'$i'_'$z'_HS.txt/g' correlation.C
    sed -i 's/INPUT_FILE_X/EvtEff_FIN_VAR_'$i'_'$z'_HS.txt/g' correlation.C
    sed -i 's/MESSAGE_LEG_1/IniBDTCut = '$i'e-1 CosSVPV = '$z'e-1 FinBDT=var/g' correlation.C
    sed -i 's/X_AXIS_LABEL/EvtEff/g' correlation.C
    sed -i 's/Y_AXIS_LABEL/'$g'/g' correlation.C
    sed -i 's/SAVE_NAME/'$g'_Var-Fin_Correlation/g' correlation.C
    sed -i 's/ROOT_FILE_NAME/'$g'/g' correlation.C

    root -q .x correlation.C
    cp Backup_correlation.C correlation.C
done







<<COMMENT

cp Backup_elaboration.C elaboration.C
#if ($index="5") #VAR = IniBDT ############################new######################################
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the FinBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    j=0 ######################################## MODIFY #####################################3333
    #i=-7
    z=4
    temp=10
    sed -i 's/float index=0/float index=5/g' elaboration.C
    sed -i 's/INPUT_FILE_1/'$b'_INI_VAR_'$j'_'$z'.txt/g' elaboration.C
    sed -i 's/MESSAGE_LEG_1/FinBDTCut = '$j'e-1 CosSVPV = '$z'e-1/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        i=`expr $f - $temp`
        sed -i 's/Plots_'$f'.root/Plots_Ini_var_'$i'.root/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/IniBTDCut = '$i'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/IniBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Ini/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_INIVAR/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=5/float index=0/g' elaboration.C




#if ($index="4") #VAR = Fin ##########################new######################
cp Backup_elaboration.C elaboration.C
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    z=4 ######################################## MODIFY #####################################
    temp=1
    t=3
    i=-7
    sed -i 's/float index=0/float index=4/g' elaboration.C
    sed -i 's/INPUT_FILE_1/'$b'_FIN_VAR_'$i'_'$z'.txt/g' elaboration.C
    sed -i 's/MESSAGE_LEG_1/IniBDTCut = '$i'e-1 CosSVPV = '$z'e-1/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        c=`expr $f - $temp`
        j=`expr $f - $t`
        sed -i 's/Plots_'$f'.root/Plots_Fin_var_'$j'.root/g' elaboration.C #Plots_Fin_var_'$i'_'$j'_'$z'.root/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/FinBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Fin/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_FINVAR/g' elaboration.C

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=4/float index=0/g' elaboration.C










#if ($index="4") #VAR = Fin ##########################new######################  LONG RANGE
cp Backup_elaboration.C elaboration.C
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the IniBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    z=4 ######################################## MODIFY #####################################
    temp=1
    t=3
    i=-7
    sed -i 's/float index=0/float index=4/g' elaboration.C
    sed -i 's/INPUT_FILE_1/'$b'_FIN_VAR_'$i'_'$z'.txt/g' elaboration.C
    sed -i 's/MESSAGE_LEG_1/IniBDTCut = '$i'e-1 CosSVPV = '$z'e-1/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
    do
        c=`expr $f - $temp`
        j=`expr $f - $t`
        sed -i 's/Plots_'$f'.root/Plots_Fin_var_'$j'.root/g' elaboration.C #Plots_Fin_var_'$i'_'$j'_'$z'.root/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/FinBDTCut = '$j'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/FinBDTCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Fin/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_FINVAR/g' elaboration.C

    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=4/float index=0/g' elaboration.C












cp Backup_elaboration.C elaboration.C
#if ($index="1") #VAR = CosSVPV ############################new######################################
for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the FinBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    j=0 ######################################## MODIFY #####################################3333
    i=-7
    #z=4
    temp=1
    sed -i 's/float index=0/float index=1/g' elaboration.C
    sed -i 's/INPUT_FILE_1/'$b'_COS_VAR_'$i'_'$j'.txt/g' elaboration.C
    sed -i 's/MESSAGE_LEG_1/FinBDTCut = '$j'e-1 IniBDTCut = '$i'e-1/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        z=`expr $f - $temp`
        sed -i 's/Plots_'$f'.root/Plots_cos_var_'$z'.root/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/CosSVPVcut = '$z'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/CosSVPVCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Cos/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_COSVAR/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=5/float index=0/g' elaboration.C


cp Backup_elaboration.C elaboration.C
#if ($index="1") #VAR = CosSVPV ############################new###################### HIGHT STAT

for b in "EvtEff" "FR1" "FR2"
do
    #echo -n "What is the fixed value for the FinBDT cut? Please, enter the number *10 (1 instead of 0.1)"
    #i = $(cat)
    j=0 ######################################## MODIFY #####################################3333
    i=-7
    #z=4
    temp=1
    sed -i 's/float index=0/float index=1/g' elaboration.C
    sed -i 's/INPUT_FILE_1/'$b'_COS_VAR_HigStat_'$i'_'$j'.txt/g' elaboration.C
    sed -i 's/MESSAGE_LEG_1/FinBDTCut = '$j'e-1 IniBDTCut = '$i'e-1/g' elaboration.C
    for f in 1 2 3 4 5 6 7 8 9 10
    do
        z=`expr $f - $temp`
        sed -i 's/Plots_'$f'.root/Plots_cos_var_'$z'.root/g' elaboration.C
        sed -i 's/MESSAGE_LEG_pt_'$f'/CosSVPVcut = '$z'e-1/g' elaboration.C
    done
    sed -i 's/Y_AXIS_LABEL/'$b'/g' elaboration.C
    sed -i 's/X_AXIS_LABEL/CosSVPVCut/g' elaboration.C
    sed -i 's/SAVE_NAME/'$b'_Var-Cos_HSTAT/g' elaboration.C
    sed -i 's/S_N_pt/PtEff_COSVAR_HSTAT/g' elaboration.C
    root -q .x elaboration.C
    cp Backup_elaboration.C elaboration.C
done
sed -i 's/float index=5/float index=0/g' elaboration.C

COMMENT


echo "The end"
