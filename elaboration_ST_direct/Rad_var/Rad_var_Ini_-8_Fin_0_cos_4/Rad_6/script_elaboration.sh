#!/bin/sh
#Autor: Beatrice Cervato
#Changing some var. names to obtain the desidered analysis

#Please, copy the analysis that you whant to perform and paste it in the uncommented part of the script.
#REMEMBER to modify the value of j, i or z.....

echo "What analysis do you want to perform? Please, enter:"
echo "1 for cos variable, IniBDT = parameter"
echo "2 for cos variable, FinBDT = parameter"
echo "3 for FinBDT variable, cos = parameter"
echo "4 for FinBDT variable, IniBDT = parameter"
echo "5 for IniBDT variable, cos = parameter"
echo "6 for IniBDT variable, FinBDT = parameter"

#index = $(cat)
#read index
echo "Fine prima parte"
cp Backup_correlation.C correlation.C
for g in "FR1" "FR2"
do
	echo "Ciao"
    ##########################    FRs VS Eff    ####################
	z=4 ######################################## MODIFY #####################################
    t=3
    i=-7
    sed -i 's/INPUT_FILE_Y/'$g'_FIN_VAR_'$i'_'$z'_HS.txt/g' correlation.C
    sed -i 's/INPUT_FILE_X/EvtEff_FIN_VAR_'$i'_'$z'_HS.txt/g' correlation.C
    sed -i 's/MESSAGE_LEG_1/IniBDTCut = '$i'e-1 CosSVPV = '$z'e-1 FinBDT=var/g' correlation.C
    sed -i 's/X_AXIS_LABEL/EvtEff/g' correlation.C
    sed -i 's/Y_AXIS_LABEL/'$g'/g' correlation.C
    sed -i 's/SAVE_NAME/'$g'_Var-Fin_Correlation/g' correlation.C
    sed -i 's/ROOT_FILE_NAME/'$g'/g' correlation.C

    root -q .x correlation.C
    cp Backup_correlation.C correlation.C
done



echo "The end"
