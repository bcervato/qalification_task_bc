#include <fstream>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TF1.h"
#include "TMultiGraph.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TPDF.h"
#include "TMath.h"
#include "TLegend.h"

#include "TGedPatternSelect.h"
#include "TGColorSelect.h"
#include "TGColorDialog.h"
#include "TColor.h"
#include "TTree.h"
#include "TTreePlayer.h"
#include "TSelectorDraw.h"
#include "TGTab.h"
#include "TGFrame.h"
#include "TGMsgBox.h"
#include "TClass.h"
#include "TLegend.h"
#include "TColor.h"
#include "TStyle.h"
#include "TGaxis.h"
#include "TEfficiency.h"

#include "THStack.h"
#include "TText.h"


using namespace std;
void elaboration ()
{
	
	    #ifdef __CINT__
    gROOT->LoadMacro("AtlasLabels.C");
    gROOT->LoadMacro("AtlasUtils.C");
	#endif

   SetAtlasStyle();
    gStyle->SetErrorX(0);
    //TGraphErrors *g_VAR1 = new TGraphErrors();
    TGraph *g_VAR1 = new TGraph();
	
    TEfficiency *h1;
    TEfficiency *h2;
    TEfficiency *h3;
    TEfficiency *h4;
    TEfficiency *h5;
    TEfficiency *h6;
    TEfficiency *h7;
    TEfficiency *h8;
    TEfficiency *h9;
    TEfficiency *h10;
    
	TH1F *hN = new TH1F("NUM", "Numerator", 21, -0.9, 1.2);
	TH1F *hD = new TH1F("DENO", "Denominator", 21, -0.9, 1.2);

	float e=0;
    int i=0;
    float index=0;
    
    ifstream fin1("INPUT_FILE_1"); 
    ifstream finN("INPUT_FILE_2"); 
    ifstream finD("INPUT_FILE_3");  
	TFile* file_01 = TFile::Open("Plots_1.root");
	TFile* file_02 = TFile::Open("Plots_2.root");
	TFile* file_03 = TFile::Open("Plots_3.root");
	TFile* file_04 = TFile::Open("Plots_4.root");
	TFile* file_05 = TFile::Open("Plots_5.root");
	/*TFile* file_06 = TFile::Open("Plots_6.root");
	TFile* file_07 = TFile::Open("Plots_7.root");
	TFile* file_08 = TFile::Open("Plots_8.root");
	TFile* file_09 = TFile::Open("Plots_9.root");
	TFile* file_10 = TFile::Open("Plots_10.root");*/
	file_01->GetObject("bPtAll_clone", h1);
	file_02->GetObject("bPtAll_clone", h2);
	file_03->GetObject("bPtAll_clone", h3);
	file_04->GetObject("bPtAll_clone", h4);	
	file_05->GetObject("bPtAll_clone", h5); 	
	/*file_06->GetObject("bPtAll_clone", h6);	
	file_07->GetObject("bPtAll_clone", h7);
	file_08->GetObject("bPtAll_clone", h8);
	file_09->GetObject("bPtAll_clone", h9);
	file_10->GetObject("bPtAll_clone", h10);*/
	h1->SetLineColor(kOrange-3);
	h2->SetLineColor(kRed-4);
	h3->SetLineColor(kGreen-3);
	h4->SetLineColor(kAzure+10);
	h5->SetLineColor(kBlue-4);
	/*h6->SetLineColor(kCyan+2);
	h7->SetLineColor(kViolet+1);
	h8->SetLineColor(kRed+3);
	h9->SetLineColor(kGreen+3);
	h10->SetLineColor(kYellow-8);	*/
	h1->SetMarkerColor(kOrange-3);
	h2->SetMarkerColor(kRed-4);
	h3->SetMarkerColor(kGreen-3);
	h4->SetMarkerColor(kAzure+10);
	h5->SetMarkerColor(kBlue-4);
	/*h6->SetMarkerColor(kCyan+2);
	h7->SetMarkerColor(kViolet+1);
	h8->SetMarkerColor(kRed+3);
	h9->SetMarkerColor(kGreen+3);
	h10->SetMarkerColor(kYellow-8);
    */
    
    
    if (index == 1 || index == 2) //cos variable, IniBDT = parameter || FinBDT = parameter
    {
        i=0;
        while (i<=9)
        {
            fin1>>e;
            g_VAR1->SetPoint(i,i*0.1,e);
            //g_VAR1->SetPointError(i, 0,sqrt(e));
            i++;
        };  
        
    }
 
    if (index == 3 || index == 4)// FinBDT variable, cos = parameter || IniBTD= parameter
    {
		i=0;
        while (i<=19)
        {
            fin1>>e;
            g_VAR1->SetPoint(i,(i-9)*0.1,e);
            i++;
        };
        i=0;
        while (i<=19)
        {
            finN>>e;
            hN->SetBinContent(i,e);
            i++;
        }; 
        i=0;
        while (i<=19)
        {
            finD>>e;
            hD->SetBinContent(i,e);
            i++;
        };
    }

    if (index == 5 || index == 6)// IniBDT variable, cos = parameter || FinBDT = parameter
    {
		i=0;
        while (i<=9)
        {
            fin1>>e;
            g_VAR1->SetPoint(i,(i-9)*0.1,e);
            //g_VAR1->SetPointError(i, 0,sqrt(e));
            i++;
        };  
        
     }
		
		
    g_VAR1->SetMarkerColor(kOrange-3);

    
    TMultiGraph *mg = new TMultiGraph();

    mg->Add(g_VAR1,"cp");


	//mg->GetYaxis()->SetRangeUser(0,0.2);
    mg->GetXaxis()->SetTitle("X_AXIS_LABEL");
    mg->GetYaxis()->SetTitle("Y_AXIS_LABEL");
    hN->GetXaxis()->SetTitle("X_AXIS_LABEL");
    hN->GetYaxis()->SetTitle("Y_AXIS_LABEL");
    hD->GetXaxis()->SetTitle("X_AXIS_LABEL");
    hD->GetYaxis()->SetTitle("Y_AXIS_LABEL");
    
    
    TEfficiency* pEff = 0;
    TFile* pFile = new TFile("myfile.root","recreate");
	//h_pass and h_total are valid and consistent histograms
	if(TEfficiency::CheckConsistency(*hN,*hD))
	{
		pEff = new TEfficiency(*hN,*hD);
		pEff->Write();
		TCanvas *cseff = new TCanvas("cseff","cseff",10,10,1100,900);
		pEff->SetMarkerColor(0);
		pEff->SetMarkerStyle(15);
		pEff->Draw("Z AB");
		cseff->SaveAs("SAVE_NAME_EFF.png"); 
	  // this will write the TEfficiency object to "myfile.root"
	  // AND pEff will be attached to the current directory
	}
		



    TCanvas *cs = new TCanvas("cs","cs",10,10,1500,900);
    mg->Draw("a");
    TLegend *leg = new TLegend(); //0.12,0.75,0.38,0.9
    leg->SetBorderSize(0);
	leg->AddEntry(g_VAR1,"MESSAGE_LEG_1","P");


	leg->SetFillColor(kWhite);
	leg->Draw();
    cs->SaveAs("SAVE_NAME.png"); 
    
    
    
    
    /*TMultiGraph *mgEff = new TMultiGraph();

    mgEff->Add(h1,"Z");
    mgEff->Add(h2,"Z");
    mgEff->Add(h3,"Z");
    mgEff->Add(h4,"Z");
    mgEff->Add(h5,"Z");
    mgEff->Add(h6,"Z");
    mgEff->Add(h7,"Z");
    mgEff->Add(h8,"Z");
    mgEff->Add(h9,"Z");
    mgEff->Add(h10,"Z");
    mgEff->Add(h11,"Z");
    mgEff->Add(h12,"Z");
    mgEff->Add(h13,"Z");
    mgEff->Add(h14,"Z");
    mgEff->Add(h15,"Z");
    mgEff->Add(h16,"Z");
    mgEff->Add(h17,"Z");
    mgEff->Add(h18,"Z");
    mgEff->Add(h19,"Z");
    mgEff->Add(h20,"Z");
    */
    
    h1->SetTitle("BH-reconstruction Efficiency;BH Pt [MeV];#epsilon");
    
	TGaxis::SetMaxDigits(3);
	TCanvas *cs1 = new TCanvas("cs1","cs1",10,10,1900,1000);
    //mg->Draw();
    //h1->SetMarkerStyle(0);
    h1->Draw("AP");
	//h2->SetMarkerStyle(0);
    h2->Draw("SAME");
	//h3->SetMarkerStyle(0);
    h3->Draw("SAME");
	//h4->SetMarkerStyle(0);
    h4->Draw("SAME");
	//h5->SetMarkerStyle(0);
	h5->Draw("SAME");
	//h6->SetMarkerStyle(0);
	/*h6->Draw("SAME");
	//h7->SetMarkerStyle(0);
	h7->Draw("SAME");
	//h8->SetMarkerStyle(0);
	h8->Draw("SAME");
	//h9->SetMarkerStyle(0);
	h9->Draw("SAME");
	//h10->SetMarkerStyle(0);
	h10->Draw("SAME");
	*/
		
    gPad->Update(); 
	auto graph3 = h1->GetPaintedGraph(); 
	graph3->SetMinimum(0);
	graph3->SetMaximum(1); 
	gPad->Update();
    
    TLegend *leg1 = new TLegend(0.2,0.75,0.38,0.9); //0.12,0.75,0.38,0.9
    leg1->SetBorderSize(0);
	leg1->AddEntry(h1,"MESSAGE_LEG_pt_1","F");
	leg1->AddEntry(h2,"MESSAGE_LEG_pt_2","F");
	leg1->AddEntry(h3,"MESSAGE_LEG_pt_3","F");
    leg1->AddEntry(h4,"MESSAGE_LEG_pt_4","F");
    leg1->AddEntry(h5,"MESSAGE_LEG_pt_5","F");
	/*leg1->AddEntry(h6,"MESSAGE_LEG_pt_6","F");
	leg1->AddEntry(h7,"MESSAGE_LEG_pt_7","F");
	leg1->AddEntry(h8,"MESSAGE_LEG_pt_8","F");
	leg1->AddEntry(h9,"MESSAGE_LEG_pt_9","F");
	leg1->AddEntry(h10,"MESSAGE_LEG_pt_10","F");*/
	leg1->SetFillColor(kWhite);
	leg1->Draw();
    cs1->SaveAs("S_N_pt.png");
	
	
	
	
	
	
	/* 	Bin content NOT one on the top of each other BUT one next to each other.
	 * 
	 * 
	 * 
    TCanvas *cs1 = new TCanvas("cs1","cs1",10,10,4000,1000);
    
	//h1->GetXaxis()->SetNdivisions(1000);
	//h1->SetFillColor(4);
	h1->SetBarWidth(0.25);
	h1->SetBarOffset(0);
	h1->SetFillColor(kOrange-2);
	//h1->SetBarOffset(0.1);
	//h1->SetStats(0);
    h1->SetMarkerStyle(0);
    h1->Draw("bar E X0");
    
    h2->SetFillColor(kRed-10);
    h2->SetBarWidth(0.25);
	h2->SetMarkerStyle(0);
	h2->SetBarOffset(0.25);
    h2->Draw("bar E SAME X0");
    
    h3->SetBarWidth(0.25);
    h3->SetBarOffset(0.5);
	h3->SetMarkerStyle(0);
	h3->SetFillColor(kGreen-10);
    h3->Draw("bar E SAME X0");
    
    h4->SetFillColor(kCyan-10);
    h4->SetBarWidth(0.25);
    h4->SetBarOffset(0.75);
	h4->SetMarkerStyle(0);
    h4->Draw("bar E SAME X0");
     
    if (index==2 || index==6)
    {
		h5->SetMarkerStyle(0);
		h5->SetFillColor(kBlue-10);
		h5->Draw("HIST E SAME");
	}
	if (index==5 || index==3)
    {
		h5->SetMarkerStyle(0);
		h5->SetFillColor(kBlue-10);
		h5->Draw("HIST E SAME");
		
		h6->SetFillColor(kCyan-9);
		h6->SetMarkerStyle(0);
		h6->Draw("HIST E SAME");
		
		h7->SetFillColor(kMagenta-10);
		h7->SetMarkerStyle(0);
		h7->Draw("HIST E SAME");
	}
		
    
    TLegend *leg1 = new TLegend(0.12,0.75,0.38,0.9); //0.12,0.75,0.38,0.9
    leg1->SetBorderSize(0);
	leg1->AddEntry(h1,"MESSAGE_LEG_pt_1","F");
	leg1->AddEntry(h2,"MESSAGE_LEG_pt_2","F");
	leg1->AddEntry(h3,"MESSAGE_LEG_pt_3","F");
    leg1->AddEntry(h4,"MESSAGE_LEG_pt_4","F");
    if (index==2 || index==6)
        leg1->AddEntry(h5,"MESSAGE_LEG_pt_5","F");
    if (index==3 || index==5){
        leg1->AddEntry(h5,"MESSAGE_LEG_pt_5","F");
        leg1->AddEntry(h6,"MESSAGE_LEG_pt_6","F");
        leg1->AddEntry(h7,"MESSAGE_LEG_pt_7","F");
    }
	leg1->SetFillColor(kWhite);
	leg1->Draw();
    cs1->SaveAs("S_N_pt.png");
 */
  return 0;
}


